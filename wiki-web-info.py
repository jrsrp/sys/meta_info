#!/usr/bin/env python
"""
This script does a bit of HTML scraping
in order to import targeted tables from old
QLD wiki site on:
https://rsc-wiki.dnr.qld.gov.au/doku.php?id=start
"""
import re
import logging

from bs4 import BeautifulSoup
import click
from requests_cache import CachedSession

try:
    import yaml
    from yaml import CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeDumper


logger = logging.getLogger(__name__)

QVF_NAME_CODES_URL = "https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:filename_codes#file_naming_codes"


@click.group()
@click.pass_context  # I need this to use some options in sub-commands.
@click.option(
    "--url",
    required=True,
    default=QVF_NAME_CODES_URL,
    show_default=True,
    help=(
        "Required. URL where HTML search will be done. "
        "Defaults to qvf filename codes url on rsc-wiki.dnr.qld.gov.au"
    ),
)
@click.option(
    "--verbosity",
    default="info",
    show_default=True,
    help=("Optional: set verbosity level to warning, info, or debug."),
)
def main(ctx: click.Context, url: str, verbosity: bool) -> None:
    """main CLI entry point for querying old Wiki page
    \f
    Args:\n
      ctx (click.Context): context object for passing
    internal objects between click cmds.\n
      url (str): target html page to conduct search.
    verbosity (bool): control for verbosity level
    """
    # ensure that ctx.obj exists and is a dict
    # (in case `cli()` is called by other means than
    # if __name__ == '__main__': main(obj={})
    ctx.ensure_object(dict)
    ctx.obj["URL"] = url

    if verbosity == "info":
        log_level = logging.INFO
    elif verbosity == "debug":
        log_level = logging.DEBUG

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    logger.addHandler(ch)


def get_stage_info(soup: BeautifulSoup, id: tuple[str]) -> dict:
    """Get QVF stage and description from old wiki site.
    It works by getting the HTML table that follows after
    a given HTML tag.
    This function only supports querying a two-column table.
    TODO: Adapt it to allow parsing of nested JSON fields
          into YAML tables.

    Initially thought for searching qvf stages, but also
    works for any given search term.

    Args:
        soup (bs4.BeautifulSoup): parsed html object.
        id (tuple[str]): tuple with search targets
          - html tag type name.
          - string to search for within html tag.

    Returns:
        dict: qvf stage description dictionary.
    """
    # mark2 = soup.find("h3", {"id": id})
    html_tag, search_str = id
    mark = soup.find(
        lambda tag: tag.name == f"{html_tag}" and f"{search_str}" in tag.text.lower()
    )

    stagetable = mark.find_next("table")
    trs = stagetable.find_all("tr")
    stagedict = {}
    for row in trs:
        if row.find_all("th"):  # skip header row
            continue
        cell1, cell2 = row.find_all("td")
        stage = cell1.get_text(strip=True)
        description = cell2.get_text(strip=True)
        stagedict[stage] = description
        # this is redundant, but leaving it here in case is useful later
        stage_pattern = re.compile(r"[a-zA-Z0-9]{3}")
        if re.search(stage_pattern, stage):
            stagedict[stage] = description

    return stagedict


# export a given table to yml format
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@main.command(
    name="export-table",
    context_settings=CONTEXT_SETTINGS,
    short_help="export a given HTML table to YAML format.",
)
@click.pass_context
@click.argument(
    "table_id",
    nargs=2,
    required=True,
)
@click.option(
    "--out-yml",
    default=None,
    help=(
        "Name of output file name. If None, it will use same "
        "prefix from table id search string in HTML tag."
    ),
)
def export_table(ctx, table_id, out_yml):
    """Given a tuple argument with:\n
        - html tag type name.\n
        - string to search for within html tag.

    export the target table to a yaml file.

    Args:\n
        table_id (tuple[str]): tuple, see description above.\n
        out_yml (str): output filename.
    """

    url = ctx.obj["URL"]
    # caching responses with low frequency of change
    s = CachedSession(
        cache_name="rsc_wiki_cache",
        backend="sqlite",
        expire_after=3600,
        use_cache_dir=True,
    )
    if "rsc-wiki.dnr.qld.gov.au" in url:
        s.trust_env = False
        s.verify = False
    html = s.get(url).content
    soup = BeautifulSoup(html, "html.parser")
    stages_dict = get_stage_info(soup, table_id)
    if out_yml is None:
        tag, search_str = table_id
        non_alphanum_pat = r"[^A-Za-z0-9]+"
        out_name = re.sub(non_alphanum_pat, "_", search_str).lower()
        out_yml = f"{out_name}_tbl.yml"
        if out_name.endswith("_"):
            out_yml = f"{out_name}tbl.yml"
    opts = {"indent": 5, "width": 90, "version": (1, 1)}
    with open(out_yml, mode="wt", encoding="utf-8") as f:
        yaml.dump(stages_dict, f, Dumper=MyDumper, **opts)


class MyDumper(yaml.SafeDumper):
    # HACK: insert blank lines between top-level objects/scalars
    # https://github.com/yaml/pyyaml/issues/127
    # inspired by https://stackoverflow.com/a/44284819/3786245
    def write_line_break(self, data=None):
        super().write_line_break(data)

        if len(self.indents) == 1:
            super().write_line_break()


if __name__ == "__main__":
    main(obj={})
