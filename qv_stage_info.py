#!/usr/bin/env python
"""
Query a given QV table from YAML config files.
"""

import sys
import re
import click
import yaml
from requests_cache import CachedSession
from rich.console import Console
from rich.table import Table


QV_STAGES_TABLES = {
    "usgs_landsat": "https://gitlab.com/jrsrp/sys/meta_info/-/raw/main/usgs_landsat_stage_codes_tbl.yml",
    "sentinel2": "https://gitlab.com/jrsrp/sys/meta_info/-/raw/main/sentinel_2_stages_tbl.yml",
    "Earth-I": "https://gitlab.com/jrsrp/sys/meta_info/-/raw/main/earthi_stages_tbl.yml",
}


def search_report(
    qv_items: dict,
    term: str,
    table_name: str = None,
    re_escape: bool = False,
    search_desc: bool = False,
) -> None:
    """Pretty print to stdout config yaml search results using
    rich formatting.

    Args:
        qv_items (dict): dictionary with qvf stages.
        table_name (str): assign a name to rich output table
        term (str): term/word to search for.
        re_escape (bool): use re.escape to ignore regex metacharacters.
        search_desc (bool): search term in description
          field of the target table.

    """
    table = Table(title=f"{table_name.lower()} results")
    table.add_column("stage", justify="right", style="bold red", no_wrap=True)
    table.add_column("description")

    table_is_valid = False
    if search_desc:
        # then we search for the string in the values of the dictionary
        for key, desc in qv_items.items():
            if amatch := re.search(term, desc, re.IGNORECASE):
                prestring = desc[0 : amatch.span()[0]]
                matchstring = amatch.group()
                poststring = desc[amatch.span()[1] :]
                table.add_row(
                    key, f"{prestring}[gold1]{matchstring}[/gold1]{poststring}"
                )
                table_is_valid = True
    else:
        try:
            search_term = term
            if re_escape:
                search_term = re.escape(term)
            for key, desc in qv_items.items():
                # checks for a match only at the beginning of the string
                if amatch := re.match(search_term, key, re.IGNORECASE):
                    stage_res = qv_items[key]
                    table.add_row(key, stage_res)
                    table_is_valid = True
        except KeyError:
            pass
    # only print if we have something to print
    if table_is_valid:
        console = Console()
        console.print(table)


CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(
    name="qv-query",
    context_settings=CONTEXT_SETTINGS,
    short_help="query a given QV table from YAML config file.",
)
@click.argument("term")
@click.option(
    "-t",
    "--qv-table",
    default=None,
    type=click.Choice(QV_STAGES_TABLES.keys(), case_sensitive=False),
    help="Name of QV yml table to search in.",
)
@click.option(
    "--search-desc",
    is_flag=True,
    default=False,
    help="search for term in description field of table(s).",
)
@click.option(
    "--re-escape",
    is_flag=True,
    default=False,
    help="Whether to ignore regex metacharacters in search term.",
)
def stage_info(term, qv_table, search_desc, re_escape):
    """
    Given a qvf stage or search term, let us get some description.
    Initially designed for searching either Sentinel-2 or Landsat d-stage tables.
    \f
    Args:
        term (str): _description_
        qv_table (str): _description_
        search_desc (bool): _description_
        re_escape (bool): _description_
    """
    # if not search-desc and table is none,
    # then we need qv table from cmdline option
    if not search_desc and qv_table is None:
        sys.exit("ERROR: Need a table name if not searching description field.")

    # caching responses with low frequency of change
    # https://webscraping.ai/faq/apis/what-are-the-best-practices-for-caching-api-responses-in-web-scraping
    s = CachedSession(
        cache_name="qv_meta_info_cache",
        backend="sqlite",
        expire_after=600,
        use_cache_dir=True,
    )
    if search_desc:
        if qv_table is None:
            tables = QV_STAGES_TABLES.keys()
        else:
            tables = [qv_table.lower()]
        for t in tables:
            url = QV_STAGES_TABLES[t]
            response = s.get(url, allow_redirects=True)
            content = response.content.decode("utf-8")
            d = yaml.safe_load(content)
            search_report(
                qv_items=d,
                term=term,
                table_name=t,
                re_escape=re_escape,
                search_desc=True,
            )
    else:
        url = QV_STAGES_TABLES[qv_table.lower()]
        response = s.get(url, allow_redirects=True)
        content = response.content.decode("utf-8")
        d = yaml.safe_load(content)
        search_report(
            qv_items=d,
            term=term,
            table_name=qv_table,
            re_escape=re_escape,
            search_desc=False,
        )


if __name__ == "__main__":
    stage_info()
