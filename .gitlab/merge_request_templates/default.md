- [ ] I read the project's [contribution guide](docs/CONTRIBUTING.md).
- [ ] I confirm that the static html file(s) in [test build artifacts](https://jrsrp.gitlab.io/sys/meta_info/CONTRIBUTING/#where-relevant-we-use-github-flow-to-manage-source-content) render without errors.
- [ ] I added an entry to `CHANGELOG.md` if knowledge of this change could be valuable to users.

