# Filenaming Convention for JRSRP files

The Joint Remote Sensing Research Project ([JRSRP](https://www.jrsrp.org.au/)) uses a filenaming convention as a way to quickly provide metadata about a file's provenence.

This project contains the data for these codes, and the [documentation](https://jrsrp.gitlab.io/sys/meta_info/) to help interpret them. 
