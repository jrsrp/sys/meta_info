from itertools import chain
from pathlib import Path

import yaml
import pandas as pd
from mkdocs.config.defaults import MkDocsConfig


def on_pre_build(config: MkDocsConfig, **kwargs) -> None:
    """Transpose yaml config data sets to csv before building docs."""
    root_dir = Path(config.config_file_path).parent
    patterns = ['*tbl.yml', '*tbl.yaml']
    found_tables = list(
        chain.from_iterable(
            [root_dir.glob(p) for p in patterns]
        )
    )
    for tbl in found_tables:
        with open(tbl, mode='rt', encoding='utf-8') as f:
            tbl_dict = yaml.safe_load(f)

        df = pd.json_normalize(tbl_dict)
        dest_csv = root_dir / 'docs' / 'tables_import' / f'{tbl.stem}.csv'
        df.T.to_csv(dest_csv, header=None)
