## Contributing to JRSRP meta_info

This project manages key metadata information using configuration files that can then be easily managed,<br>
shared, inspected and incorporated into JRSRP systems, [see importing into yaml tables.](README.md#exporting-old-dokuwiki-tables-into-yaml-configuration-files)

## Adding a New Stage

Maintainers and owners can push directly to the main branch, but for other contributors, we recommend using
the [Github Flow](https://docs.github.com/en/get-started/quickstart/github-flow) 
approach of first making an issue, followed by a merge request.

Then, simply edit the table that corresponds to your product. For example, adding a new Sentinel2 stage, 
simply modify the `sentinel_2_stages_tbl.yml` file. 

For example, say you have a new stage in mind. Look at the yaml file, and identify a gap in the alphabetic
naming table. For example, when writing this document, a part of the table looks like:


```yaml
ala: Land condition derived from Peter's model using all available LCAT data at time of production.
     Masked using closest acu product and current reef grazing mask

at*: Reserved for tillage trial for land management targets

aw0: Footprint mask of standard ESA tile outline.
```

This means that a sensible new name could ba `am0`, and your edit might look like:

```yaml
ala: Land condition derived from Peter's model using all available LCAT data at time of production.
     Masked using closest acu product and current reef grazing mask

am0: My new sentinel2 product. This is a description. Try to make it succinct but informative.
     You can go over multiple lines. 

at*: Reserved for tillage trial for land management targets

aw0: Footprint mask of standard ESA tile outline.
```

Once you commit and push, CI/CD will run a test of making the document. If that succeeds, you
can merge (or get someone else to merge it for you), and this web page will automatically be
recreated. 



## Where relevant, we use [Github Flow](https://docs.github.com/en/get-started/quickstart/github-flow) to manage source content
[Pull/Merge requests](https://docs.github.com/en/get-started/quickstart/github-flow#create-a-pull-request) are the best way to propose changes to yaml configuration files related to JRSRP/QVF filename/stage system:


1. Clone the repo.
2. From the web page, create an issue which summarises the new QVF metadata feature/issue you'd like addressed.
3. From the issue page, click on "create merge request". This will make a merge request and a branch.
4. Checkout this new branch and make your changes.
5. Make sure your YAML format lints:
    - Some useful formatting guides:
        - https://yaml.org/.
        - https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started.
    - If using VS Code IDE the [yaml extension](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) has a good suite of linting options.
4. Ensure the test website (triggered via CI/CD pipeline) doesn't contain any errors:
    - Push your commits to the equivalent remote branch.
    - Go to the pipeline job via Build -> Jobs.
    - Select the relevant "#job_number:test" job.
    - Go to the right hand side "Job artifacts" section and select "Browse".
    - Inspect the target static page(s) to confirm your changes.
6. When you are ready for review and merging, click on 'mark as ready' on the merge request page.

### Other changes which should follow the GitHub Flow workflow
* Adding a new page to the site via a new markdown (.md) file in the docs/ directory.

### inserting yaml config contents into markdown source
We use the handy [mkdocs table-reader plugin](https://squidfunk.github.io/mkdocs-material/reference/data-tables/#import-table-from-file)
for inserting yml content as tables on a webpage:<br>
* A routine in `scripts/hooks.py` uses the Pandas package to read the yaml content into a DataFrame and exporting it as a csv file.<br>
* The csv file is then referenced in the source markdown file, [see plugin usage.](https://timvink.github.io/mkdocs-table-reader-plugin/#usage)

### Exceptions to GitHub Flow workflow
Members having at least [Maintainer role](https://docs.gitlab.com/ee/user/permissions.html) can directly submit changes to the Main branch. This is so that we can easily apply changes related to:<br>
* Non-qvf meta config files changes, e.g. changing non-config sections of a webpage.<br>
* Minor Markdown syntax fixes.

## Maintaining a changelog of changes
A changelog entry should be included with your MR (before final merge) for the following cases:

* Addition/removal of key JRSRP/QVF config metadata via .yml files.<br>
* Addition/removal of new webpages via .md files.<br>
* Important JRSRP/QVF config metadata changes (e.g. changing the purpose of a given QVF stage) via .yml files.<br>
* Software related fixes/changes.

