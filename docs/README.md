# Filenaming Convention for JRSRP files

The Joint Remote Sensing Project (JRSRP) uses a filenaming convention as
a way to quickly provide metadata about that file's provenance.

The basic approach is for a file to have key components arranged as:

**what_where_when_processing\[\_optional\]\[.suffix\]**

This site contains information on that structure, and provides tables summarising
all the components used by JRSRP

For complete details on how this works, skip to [Filename Codes](filename_codes.md) section.

The two most commonly used tables are:

* [USGS Landsat codes](filename_codes.md#usgs-landsat-stage-codes)
* [Sentinel2 codes](filename_codes.md#sentinel-2-stages)
* [Earth-i codes](filename_codes.md#earth-i-stages)
