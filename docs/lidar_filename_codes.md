# File name convention for LiDAR data

This page was created separately from
[filename_codes](https://jrsrp.gitlab.io/sys/meta_info/filename_codes/) for ease of reading. Be
aware changes to these stage codes will impact both NSW and QLD data
repositories.

Best contact: nicholas.goodwin@des.qld.gov.au

## Considerations for the convention

The filenaming and stagenames are linked to the processing chain. Please
review in the context of your own data and add/edit comments below.

1. This follows the current convention (for raster data) as closely as practical, which is to simply translate the file from one format to another and give it a proper name. The original data format (LAS,ASCII) is compressed into a single file (.gz or .laz with a .tar container) and saved to the filestore

2.  One issue that still requires more thought is to standardise the
    elevation between acquisitions spatially and temporally:

    * It is possible we will encounter data in ellipsoid, and different geoid (~AHD) heights.
    * There are also three AusGeoid models present in lidar datasets floating around now: Ausgeoid98, 09 and 2020
    * It would be prudent to store a copy of lidar data in ellipsoid heights, especially for change detection and application of improved geodetic processing where necessary.
    * TODO: Conversion procedures to be developed. For most vegetation applications, it is not currently considered an issue.

## Naming Structure

The filenaming structure is as follows:

**what_where_when_processing\[\_optional\]\[.suffix\]**

**NOTES:**

\_optional resolution field now included in all current processing
outputs e.g. r100cm.

Raw data (as received from the provider) retains their original
filenames as projects metadata are often linked to these.

All dates are based on the Universal Time at capture.

NO CAPITALS are to be used in the filenames.

Please follow the links above to see a listing of the various codes used
in each name component.

### What

**ssiipp** where ss = satellite, ii = instrument, pp = product

**Satellite (ss)**

| Code | Name              |
|:-----|-------------------|
| is   | IceSAT            |
| ap   | airborne platform |
| gp   | ground platform   |
| sb   | spaceborne        |

**Instrument (ii)** Intruments are upgraded or replaced every 3-5 years,
so we might run out of numbers in a decade or so.

| Code | Name                                           |
|:-----|------------------------------------------------|
| gl   | Geoscience Laser Altimeter System (GLAS)       |
| o1   | Optech ALTM 1025                               |
| o2   | Optech ALTM 3025                               |
| o3   | Optech ALTM 3100                               |
| o4   | Optech ALTM Gemeni                             |
| o5   | Optech ALTM Pegasus                            |
| o6   | Optech ALTM Titan                              |
| o7   | Optech ALTM Orion M200                         |
| l1   | Leica ALS-50                                   |
| l2   | Leica ALS-60                                   |
| l3   | Leica ALS-70                                   |
| l4   | Leica ALS-80                                   |
| s1   | Leica ScanStation-I                            |
| s2   | Leica ScanStation-II                           |
| s3   | Leica C5                                       |
| s4   | Leica C10                                      |
| s5   | Leica HDS-7000                                 |
| r1   | RIEGL LMS-Q560                                 |
| r2   | RIEGL LMS-Q680i                                |
| r3   | RIEGL LMS-Q240i                                |
| r4   | RIEGL LMS-Q680i-S                              |
| r5   | RIEGL LMS-Q780                                 |
| r6   | RIEGL Q1560                                    |
| r7   | RIEGL LMS-780i                                 |
| r8   | RIEGL LMS-780ii                                |
| t1   | Trimble Harrier 68i                            |
| t2   | Trimble AX60                                   |
| t3   | Toposys Harrier 56                             |
| t4   | Trimble AX60i                                  |
| v1   | Riegl VZ-400                                   |
| v2   | Riegl VZ-1000                                  |
| v3   | Riegl VZ-400i                                  |
| f1   | Faro Photon                                    |
| f2   | Faro Focus 3D 120                              |
| sa   | SALCA TLS (Salford Uni)                        |
| ec   | Echidna TLS (CSIRO)                            |
| do   | DWEL TLS (CSIRO OzDWEL)                        |
| dn   | DWEL TLS (BU NSF DWEL)                         |
| c1   | CBL-1 TLS (UBM/UML)                            |
| c2   | CBL-2 TLS (UBM/UML/UQ)                         |
| u1   | RieglminiVUX-1UAV                              |
| mp   | multiple instruments (specified in metadata)   |
| uk   | unknown instrument (not specified in metadata) |

**Product (pp)**

| Code | Name                                 |
|:-----|--------------------------------------|
| dr   | time-of-flight discrete return lidar |
| wf   | time-of-flight waveform lidar        |
| cw   | continuous wave (phase-shift) lidar  |

### Where

Data is delivered either as independent scans (scan position for TLS;
flightpaths for ALS) or tiles (ALS only).

#### Tiled data

To allow a tile to be projected to different MGA94 zones, the tile zone
must be stored in the where filename component and the data projection
in the projection filename component.

| Satellite | Syntax              | Syntax Verbose                                        | Example             | Explanation                                                               |
|-----------|---------------------|-------------------------------------------------------|---------------------|---------------------------------------------------------------------------|
| ALS       | x??????ys???????z?? | x\<x co-ordinate\>ys\<y co-ordinate\>z\<zone number\> | x561000ys7155000z55 | Tile top-left corner of top-left pixel integer coordinates and MGA94 zone |

#### Mosaics

Mosaics are named for the region they represent. Mostly, they're created
by merging the individual tiles in a project together.

The 'where' part of a mosaic filename is a six character name,
representing the region, prefixed with an 'r'. The choice of the
location ID is semi arbitrary. For example:

- Use the first 6 characters of the place name if referring to a city or
  town. That is, Brisbane becomes brisba
- Look at the file name from the data provider, in terms of how they
  divided the region, and then simplify as best as possible with
  readability. That is, Swimmers River = swmrvr
- If the place name is less than 6 characters, then pad it with trailing
  x's. So skyxxx is for a region called sky
- Experimental captures can re-fly the same area on the same day while
  varying one or more sensor configurations. This is addressed by adding
  a number to the end of the project name. For example:
  injun1,injun2,injun3...

By adding the 'r' prefix, we get rbrisba or rswmrvr or rskyxxx.

### When

For terrestrial lidar, the most accurate time possible is often required
since the same instrument could be deployed multiple times in one daya
at single site. A time-stamp is recorded using ODK forms and often also
the scanner itself.

For ALS captures, the time refers to the time-stamp of the year(s). For
projects captured within the one year, use the format YYYY. For projects
captured across a range of years use yYYYYYYYY.

Examples are below. The most accurate code possible should be used.

| Code           | Example        | Explanation                                                                                                                      |
|----------------|----------------|----------------------------------------------------------------------------------------------------------------------------------|
| yyyymmddHHMMSS | 20060810014213 | TLS datetime by year, month, day, hour, minute, second 2006 august (08) 10 01:42:13 (UTC) - refers to the timestamp of the first |
| not applicable | 2017           | refers to a single year capture of 2017                                                                                          |
| y              | y20122013      | span of two years from 2012 to 2013                                                                                              |
| y              | y19911999      | span of eight years 1991 to 1999                                                                                                 |

## Processing stage (sss)

### a-stage

Codes are used for data imports and are intended to indicate the level
of processing of the data as received.

- raw data is renamed, compressed and saved to aa0 stage in native
  format (\*.gz for ascii; \*.laz for LAS)
  - For ALS, original filenames are retained on the Postgres spatial
    databases
  - For TLS, a container (\*.tar.gz) is used to rename and compress the
    raw data
- the map and vertical projection of the a-stage files is the same as
  the projection of the raw data

#### Airborne

| Code | Explanation                                                                                                                          |
|------|--------------------------------------------------------------------------------------------------------------------------------------|
| aa0  | Ellipsoid heights                                                                                                                    |
| aa1  | Geoid heights                                                                                                                        |
| aa2+ | aa\[01\] data where further processing has been applied (e.g. noise filtering). Processing stages will be added as they are defined. |

#### Terrestrial

| Code | Explanation                                                                                                                                           |
|------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0  | Folder containing raw TLS project and no waveforms included (generally saved as.tar.gz).                                                              |
| aa1  | Folder containing raw TLS project and waveforms included (generally saved as.tar.gz).                                                                 |
| aa2  | Folder of rdbx and transformation matrix files processed for MTA zones. Some ancillary files might be needed also?                                    |
| aa4  | Folder of processed .laz files in real world UTM coordinates. Note: PULSE DEVIATION is set to 'point_source_id' and RANGE is set to 'GPS time' field. |

### b-stage

#### Airborne

Modelling canopy gap probability, foliage projective cover and crown
projective cover from airborne lidar metrics in Australian forests and
woodlands. A Fisher, J Armston, N Goodwin, P Scarth. Remote Sensing of
Environment 237, 111520

Note: 'bbj' to 'bbl' were only applied to early datasets (before ~2010)
which had no or terrible 'bb4' classification of buildings,
vegetation,etc.

#### Terrestrial

#### Vegetation TLS Products: bd-stage

Vegetation application focused products. Terrestrial codes are still in
development and gridded products for individual scans are too
numerous/complex to be stored efficiently and probably best to recreate
them on demand. These layers/products are produced by the 'TLSVEG'
modules.

Best contact: nicholas.goodwin@des.qld.gov.au

Optional suffix options:

    'dd'= decimal degrees of scan acquisition pulse spacing
    'scan positions' = c,60,120,180,240,300 (star7) or 0,1,3... (vt_pair)
    'scan orientation' = 'v' for vertical and 't'for tilted scan.

Examples of what_where_when_processing\[\_optional\]\[.suffix\]

    gpv3dr_15061699e02627600s_20190813120000_bd0m6_r5cm.tif
    gpv3dr_15282700e02742900s_20220606234200_bdom6_r30dd_120v.tif

{{ read_csv('docs/tables_import/lidar_stages_tbl.csv') }}

### Gully TLS Products: bc stage

Gully focused output products from "rxp_gully_dem_products.py"

| Code | Explanation                                                               |
|------|---------------------------------------------------------------------------|
| bc0  | Minimum Z grid                                                            |
| bc1  | Northing locations of minimum height grid                                 |
| bc2  | Easting locations of minimum height grid                                  |
| bc3  | Median filtered minimum height grid                                       |
| bc4  | Interpolated minimum heights to regular grid with median filtered applied |
| bc5  | Pulse deviation of points used in minimum height grid                     |
| bc6  | Return number of points used in minimum height grid                       |
| bc7  | Downhill slope (using a 5x5 kernel)                                       |
| bc8  | Maximum height grid (elevation)                                           |
| bc9  | DFME (difference from mean elevation)                                     |
| bca  | Binary gully extent map                                                   |
| bcb  | Maximum height relative to ground surface                                 |

### Spaceborne lidar

N/A at the moment

### f-stage

Codes are used \*by NSW OEH) for "supplied DEMs"

| Code | Explanation                                       |
|------|---------------------------------------------------|
| fa0  | f - supplied raster, a0 - digital elevation model |

|          what           |      where       |       when        |            options            |  suffix   |
|:-----------------------:|:----------------:|:-----------------:|:-----------------------------:|:---------:|
|        ap l1 dr         |     p forbes     | d2016051720160527 |          fa0 m6 r5m           |    img    |
| platform sensor product | project location |       date        | process projection resolution | extension |

example: apl1dr_pforbes_d2016051720160527_fa0m6_r5m.img

## Map Projection (pp)

see
[filename_codes#map_projection_pp](https://jrsrp.gitlab.io/sys/meta_info/filename_codes/#map-projection-pp)

If using the alternative "where" filename code, you must also use the
map projection extension for UTM zones outside 50-59.

## Optional Components

These should be now be stored in alphabetical order according to the
first character in the option code (qvf.py is written to allow you to
ensure that the optional components are stored alphabetically).

<table>
<thead>
<tr class="header">
<th>Order</th>
<th>Code</th>
<th>Explanation</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>First</td>
<td>pssssss</td>
<td>s string.<br />
This is six letter project code used for airborne lidar projects, e.g.
gatesk is Gatton-Esk. A full list can be found on <a
href="/rsc/data_prep/lidar/filenames/project_mosaics">project_mosaics</a></td>
</tr>
<tr class="even">
<td>Second</td>
<td>annn</td>
<td>n numeric.<br />
This is the azimuth or bearing from magnetic north of a TLS scan
position relative to site centre, e.g. t00. Where several scans (at the
same site) with different azimuth values are combined, the value aall is
used.</td>
</tr>
<tr class="odd">
<td>Third</td>
<td>dnnn</td>
<td>n numeric.<br />
This is the distance of scan positions from the center scan. Where
several scans (at the same site), with different distance values are
combined, the value dall is used.</td>
</tr>
<tr class="even">
<td>Fourth</td>
<td>tnn</td>
<td>n numeric.<br />
This is the tilt angle of the TLS (typically 0 or 90), e.g. t00. Where
several scans (at the same site) with different tilt angles are
combined, the value tall is used.</td>
</tr>
<tr class="odd">
<td>Fifth</td>
<td>rnnn[cm][m][km][cd][p]</td>
<td>n numeric.<br />
This is the spatial resolution of raster products and also indicates the
SPD spatial index bin size. Uses the same convention as satellite
imagery, see <a
href="https://jrsrp.gitlab.io/sys/meta_info/filename_codes/#image-resolution">filename_codes#image_resolution</a>.</td>
</tr>
<tr class="even">
<td>Sixth</td>
<td>ni</td>
<td>i numeric.<br />
This is the repeat scan number, if repeat scans area done.</td>
</tr>
</tbody>
</table>

## Suffix

**.xxx** where xxx is the three letter file suffix

See [filename_codes#suffix](https://jrsrp.gitlab.io/sys/meta_info/filename_codes/#suffix)

Lidar specific suffixes are:

| Suffix | Description                                  |
|--------|----------------------------------------------|
| las    | ASPRS LiDAR binary format                    |
| laz    | ASPRS compressed LiDAR binary format         |
| gkw    | GeocodeWF binary point format                |
| lgc    | GeocodeWF binary geocoding format            |
| lwf    | GeocodeWF calibrated waveform file format    |
| img    | HFA format for derived raster products       |
| hdr    | ENVI header file for derived raster products |
| ptx    | Leica scan-order ASCII export format         |

## ToDo

1.  Determine standard for vertical datum
2.  Update metadata import script + sql table
