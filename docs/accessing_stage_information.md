## Querying YAML configuration files

All stage name information is held in yaml files with a simple key value structure. For example,
the first few lines of
 [sentinel_2_stages_tbl.yml](`https://gitlab.com/jrsrp/sys/meta_info/-/blob/main/sentinel_2_stages_tbl.yml?ref_type=heads`)
file look like:

```yaml
%YAML 1.1
---
aa0: Used for the per-tile XML metadata file (.meta)
aa1: Sun and satellite angles image, at 5km resolution, as created from the information in the
     .meta file
aa2: TOA Radiance, back-calculated from the TOA reflectance (10m bands), and the sun zenith
     angle. Not generally saved.
aa3: TOA Radiance, back-calculated from the TOA reflectance (20m bands), and the sun zenith
     angle. Not generally saved.
aa4: 'Auxiliary data as supplied by ESA. Layers are: Atmospheric precipitable water (kg/sq-m),
     mean sea-level pressure (Pa), total column ozone (Dobson units). This is on a 0.125 degree
     grid (i.e. roughly 12.5km pixel size).'

```

This makes it simple to programmatically access the stage code and the description for all
our images.

The following code shows how we could access the contents of one of the project's yaml files from its public URL location
and loads it into a Python dictionary object:

```python linenums="1"
import requests
import yaml

url = 'https://gitlab.com/jrsrp/sys/meta_info/-/raw/main/sentinel_2_stages_tbl.yml'
response = requests.get(url, allow_redirects=True)
content = response.content.decode("utf-8")
content = yaml.safe_load(content)

In : content['ab0']
Out : 'Level 1C TOA Reflectance as supplied by ESA, for the 10m bands. This is the equivalent of the Landsat dbe stage.'
'WARNING:prior to 2023, the abo code layer, may record inhouse-georectified or non-rectified inputs. See stage abu.'
```

We also include a tool
[`qv_stage_info.py`](https://gitlab.com/jrsrp/sys/meta_info/-/blob/main/qv_stage_info.py?ref_type=heads)
for inspecting/querying the USGS, Sentinel-2 and Earth-I yaml tables.

```shell
❯ ./qv_stage_info.py --help
Usage: qv_stage_info.py [OPTIONS] TERM

  Given a qvf stage or search term, let us get some description. Initially
  designed for searching either Sentinel-2 or Landsat d-stage tables.

Options:
  -t, --qv-table [usgs_landsat|sentinel2|Earth-I]
                                  Name of QV yml table to search in.
  --search-desc                   search for term in description field of
                                  table(s).
  --re-escape                     Whether to ignore regex metacharacters in
                                  search term.
  -h, --help                      Show this message and exit.

```
Some examples:

* Querying `ab0` Sentinel-2 stage:

![Alt text](imgs/stage_info_ab0.png)


* Using a regex wildcard for all `da*` USGS stages:

![Alt text](imgs/stage_info_regex.png)


* Also, you can disable regex special characters for dealing with ambiguous QV stage placeholders.
(More discussion is needed whether we should accept stages in this form and the new yaml format...)

```shell
❯ ./qv_stage_info.py -t usgs_landsat --re-escape 'dc*'
                           usgs_landsat results
┏━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ stage ┃ description                                                    ┃
┡━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│   dc* │ Reserved for other tree mapping products, yet to be determined │
└───────┴────────────────────────────────────────────────────────────────┘
```

* Searching for matches of 'fisher' in both USGS and Sentinel-2 tables:

![Alt text](imgs/stage_info_description.png)

