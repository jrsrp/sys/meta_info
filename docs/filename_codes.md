
# File Naming Codes

This page is a large, and perhaps poorly structured listing of all the
possible values which can appear in the different fields of the Remote
Sensing Centre's QVF filenaming convention. It shows a set of tables for
each of the filename fields, with the possible values for that field,
and their meanings. For description and discussion of the whole QVF file
naming convention itself, see [File Naming
Convention](https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:filename_convention).

This material has grown rather wildly since its inception in 2003, and
contains a certain amount of outdated information. However, we have
tried to maintain complete backwards compatability, because in principle
all of the filenames which have used this convention may still be in
use.

Lidar information is shown on a separate page [Lidar
Filenames](https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:lidar:filenames)

## Naming Structure

The filenaming structure is as follows:

**what_where_when_processing\[\_optional\]\[.suffix\]**

| Sensor            | Template                                                   | Example                                                                                       |
|-------------------|------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Landsat MSS       | l\[1-5\]msre_ssssss_yyyymmdd_ssspp.xxx                     | l2msre_372216_19800928_av2m4.img                                                              |
| Landsat TM / ETM+ | l\[5/7\]tmpp_ssss_yyyymmdd_ssspp.xxx                       | l5tmre_chat_20010101_ba7m5.img                                                                |
| MISR              | t1mipp_000111222_yyyymmdd_ssspp_svv_mx_ptt.xxx             | t1mitp_093110112_20030927_aa3r0_s22_ml_ps3.img t1mitp_093110112_20030927_aa0r0_s22_ml_pan.gic |
| PALSAR            | alpsba_ssssssss_yyyymmdd_ssspp_pxxdx_jxxxxxxxxx.xxx        | alpsba_47282542_20060701_aa1u5_pdxdd.img                                                      |
| Quickbird         | q2qbpp_ssssssss_yyyymmdd_ssspp.xxx                         | q2qbpa_52052736_20040512_aa0m6.img                                                            |
| Airborne Lidar    | lil1dr\_\[a-b\]ssssssssss_yyyymmdd_ssspp_txxxyyy_rnnnn.xxx | lil1dr_a5205227362_20040512_aa1m6_t001001.las                                                 |

**NOTES:**

All dates are based on the Universal Time at capture. NO CAPITALS are to
be used in the filenames. Please follow the links above to see a listing
of the various codes used in each name component.

------------------------------------------------------------------------

## What

**ssiipp** where ss = satellite, ii = instrument, pp = product

**Satellite (ss)**

{{ read_csv('docs/tables_import/satellite_ss_tbl.csv') }}

**Instrument (ii)**

{{ read_csv('docs/tables_import/instrument_ii_tbl.csv') }}

**Product (pp)**

{{ read_csv('docs/tables_import/product_pp_tbl.csv') }}


------------------------------------------------------------------------

## Where

Satellite dependent


| Satellite | Example | Explanation |
|--------------------------------|------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Landsat 5 TM and 7 ETM+, 8 OLI | p\[0-9\]\[0-9\]\[0-9\]r\[0-9\]\[0-9\]\[0-9\] e.g. p089r079 | USGS World Reference System (WRS2) Landsat Path and Row                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Landsat 5 TM and 7 ETM+        | ssss chat                                                  | four letter scene identifier - used when purchasing imagery from Geoscience Australia and predecessors                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Landsat 5 TM and 7 ETM+        | ssssss 543264                                              | standard scene centre expressed as longitude/latitude to one decimal place and excluding the leading "1" of the longitude (eg long = **154.3E**, lat = **26.4S** produces **543264**)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Landsat 1, 2, 3 and 5 MSS      | ssssss 543264                                              | standard scene centre expressed as longitude/latitude to one decimal place and excluding the leading "1" of the longitude (eg long = **154.3E**, lat = **26.4S** produces **543264**)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| SPOT                           | ssssssss                                                   | Scene centre expressed as longitude/latitude. First four digits are (longitude - 100) \* 100, second 4 digits are abs(latitude) \* 100                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| SPOT - SpotMaps product        | SyyExxxt                                                   | SPOT's own SpotMaps tile designation. xx is absolute value of latitude, yyy is longitude, in whole degrees. t is a tile code, one of A, B, C, D, i.e. there are four tiles in a 1 degree square                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Sentinel-2                     | t55kfp                                                     | The tile names as used by ESA. These are roughly based on the US Military Grid Reference System, but they seem to have modified it a little. Prefix is always "t", next two characters are UTM zone number of the tile, next letter is the latitude band, and the final two letters are the tile id within the zone and latitude band.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| MISR                           | 000111222 093110112                                        | WRS-2 Path (000), Start block (0-180 inclusive)(111) , End block (0-180 inclusive)(222)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Quickbird                      | ssssssss 54342647                                          | scene centre expressed as longitude/latitude to two decimal places and excluding the leading "1" of the longitude (eg long = **154.34E**, lat = **26.47S** produces **54342647**) and is prefixed by the character indicating the first or second image (by timestamp) to distinguish stereo pairs where **a = first** and **b = second**. In the example, the final name is **a54342647** and **b54342647**. This referencing also applies to tiled imagery.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| PALSAR                         | ssssss 543264                                              | standard scene centre expressed as longitude/latitude to two decimal places (eg long = **154.32E**, lat = **26.42S** produces **1543202642**). We use the latitude longitude system to handle different footprints (ascending vs. descending orbit paths). The K&C strip data are expressed as RRRSUUSLL, where RSP is the reference path, SUU is the upper latitude and SLL is the lower latitude (S meaning south), e.g. 398S13S17                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| IRS-P6                         | ssssss 498343                                              | Scene centre lat/long, exactly as for Landsat5/7, i.e. 1 decimal place, drop the leading 1 in longitude, assume negative latitudes.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| All                            | aust                                                       | Australia-wide coverage of our "usual" extent (in Geographic) of whole of Australia, 112E-154E by 10S-44S                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| All                            | qld                                                        | Queensland-wide coverage of our "usual" extent (in Geographic) of whole of Queensland, 138E-154E by 10S-29.15S                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Landsat and Sentinel-2         | h14v08                                                     | The tile names used for Conditional Random Fields model outputs (a horizontal and a vertical grid coordinate).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Airborne lidar                 | ssssssssss 54342647                                        | scene centre expressed as longitude/latitude to three decimal places and excluding the leading "1" of the longitude (eg long = **154.34E**, lat = **26.47S** produces **54342647**). This position is prefixed by a character indicating the first or second image (by timestamp) to distinguish repeat flight paths on the same day where **a = first** and **b = second**. In the example, the final name is **a5434226472** and **b5434226472**. This referencing also applies to tiled lidar.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Hymap                          | ssssssssss 54342647                                        | scene centre expressed as longitude/latitude to three decimal places and excluding the leading "1" of the longitude (eg long = **154.34E**, lat = **26.47S** produces **54342647**).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| RapidEye                       | rXXXcYYY                                                   | row and column of the tile.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Deimos                         | ssssssss                                                   | Scene centre expressed as longitude/latitude. First four digits are (longitude - 100) \* 100, second 4 digits are abs(latitude) \* 100                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Mosaics                        | cnamoi06, msh5508, imallee12, ldubbo10, nsw, rwettropics   | Please refer to [Mosaic Tables](/rsc/data_prep/filename_mosaic_tables) for complete list of names. Mosaics based on state/territory or continent, catchment, mapsheet (e.g. 250k), IBRA region, local government area (LGA). catchment, mapsheet, IBRA, LGA or project are prefixed with c, m, i, l or p, respectively, and the code is at least 4-characters long making the where field at least 5 characters long. This avoids conflicts with existing 4-character scene names. Where different versions of boundary layers exist, for example IBRA v6.1 and IBRA v7, the 'where' field is appended with the year the layer was defined, for example, cnamoi06 is based on the CMA layer defined in 2006. The state/territory or continent codes are the two or three character abbreviations for the state/territory or continent without a prefix. The list is nt, qld, nsw, vic, tas, sa, wa, act, aus. An exception to this are the ADS40 image mosaics. The naming convention for ADS40 was determined prior to devising the mosaic naming convention - see below. The "r" prefix tag is a catchall for any arbitrary region names not covered by the other codes. Project "p" specific "where" components are used for airborne lidar mosaics and listed on [Mosaic Tables](/rsc/data_prep/filename_mosaic_tables). |
| ADS40 or ADS80                 | m2719, 8927, pctweedheads, 8927kf113a                      | See [filenaming_ads](/rs/data_management/filenaming_ads) for what form to use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Earth-i                        | knn or Knn                                                 | Mosaic tile reference given by Earth-i. Some inconsistencies in the tile extents between years; sometimes upper case and sometimes lower case tilename.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Sentinel-2                     | frfs0983451                                                | Fire Extent and Severity Mapping (FESM) products (NSW) use the fire incident ID, prefixed with the letter 'f' plus a 3-character code for the agency that requested the product; for example frfs0983451 for Rural Fire Service (rfs) incident 983451. The length of the incident ID field is agency-specific.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |


--------------------------------------------------------------------------------

## When


All dates should be expressed in Universal Time (GMT).

By example:

**Code Example Explanation**

| Code           | Example           | Explanation                                                                                                                                                                                                                                                                                                                                                                |
|----------------|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| yyyymmdd       | 19990923          | date by year, month, day 1999 september (09) 23 (UTC)                                                                                                                                                                                                                                                                                                                      |
| yyyy           | 2005              | For a single year, rather than a single day within the year. However, it appears that single years have also been ambiguously coded as y2005, too, which is a problem.                                                                                                                                                                                                     |
| yyyymm         | 200503            | For a single month, rather than a single day.                                                                                                                                                                                                                                                                                                                              |
| yyyymmddHHMMSS | 20060810014213    | datetime by year, month, day, hour, minute, second 2006 august (08) 10 01:42:13 (UTC)                                                                                                                                                                                                                                                                                      |
| e              | e9195             | standard epoch (eg. of vegetation change) 1991 to 1995 where the start year is in the range 1990 to 1992 and the end is 1994 to 1996                                                                                                                                                                                                                                       |
| d              | d1991032319951014 | range of days 1991 march (03) 23 to 1995 october (10) 14. The presence of the 'd' clearly defines how the 16 digits should be interpreted.                                                                                                                                                                                                                                 |
| m              | m199103199510     | range of months 1991 march (03) to 1995 october (10). The presence of the 'm' clearly defines how the 12 digits should be broken up.                                                                                                                                                                                                                                       |
| y              | y19911999         | span of eight years 1991 to 1999. The presence of the 'y' clearly distinguishes it from a single 8-digit date. Sadly the 'y' tag has been used ambiguously by some people, to denote a single year as well, so care must be taken when interpreting these fields programmatically.                                                                                         |
| n              | n20100927         | A nominal date. So far this has been used for the Landsat-7 SLC-off data. When we create a nominal image by starting with a primary one, and merging in other surrounding dates to fill the gaps, the nominal date of the image is the initial date, given in the filename, while other dates are recorded only in the history.                                            |
| s              | s0305             | A generic season name. Note that this is quite distinct from the use of the 'm' tag for specific seasons. This 's' tag is used for generic seasons, i.e. not tied to a particular year. The given example, s0305, refers to a generic autumn dataset, rather than a specific autumn of a particular year. This is most useful for things like an average over all autumns. |

**Reserved or exceptions** eall a subset of the "e" era category to be
used where the era is non-specific and can cover ALL eras.
--------------------------------------------------------------------------------

## Processing stage (sss)

SLATS data see SLATS Home for current SLATS processing stages

### Landsat "c" stream stages


Here is a summary of current SLATS Landsat processing stages, together
with the equivalent stages in the old "b" stream.

Old processing stages (from the old file naming convention, with
two-character stage codes) are preceded by the letter "a" so that stage
"c2" becomes "ac2". The "c" stage stream has used the SRTM dem in the
ortho-correction, b stage files used the previous 9 sec dem

<table>
<thead>
<tr class="header">
<th>C-stream</th>
<th>B-stream</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><strong>Template</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>aw1</td>
<td>aw1</td>
<td>Template file used to clip images down to cb4 extent</td>
</tr>
<tr class="odd">
<td><strong>Radiance/Incidence/Parameters/Radiometrics/DEM</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>ca0</td>
<td>ba0</td>
<td>Ortho-rectified imagery; 2002 baseline using SRTM (Trimmed
Edges)</td>
</tr>
<tr class="odd">
<td>ca2</td>
<td>ba2</td>
<td>Ortho-rectified imagery; using Autohotkey; registered to ca0
(untrimmed edges)</td>
</tr>
<tr class="even">
<td>ca3</td>
<td></td>
<td>Ortho-rectified imagery; using autorectify.py; registered to ca0
(untrimmed edges)</td>
</tr>
<tr class="odd">
<td>ca4</td>
<td></td>
<td>Ortho-rectified Landsat 7 SLC-off imagery; registered to ca0; uses
Near. Neighbour rather than Cubic Conv. (no need to trim edges)</td>
</tr>
<tr class="even">
<td>ca5</td>
<td></td>
<td>Ortho-rectified NSW imagery; registration to ca0 conducted by
Geoimage (edges trimmed)</td>
</tr>
<tr class="odd">
<td>ca6</td>
<td>bj2</td>
<td>Ortho-rectified imagery; purchased from ACRES as Level 10</td>
</tr>
<tr class="even">
<td>ca9</td>
<td></td>
<td>Final ortho-rectified stage; convolved edges trimmed where
applicable</td>
</tr>
<tr class="odd">
<td>caa</td>
<td></td>
<td>Mosaics of NSW ca5 imagery. Note sure if we should be doing it this
way, but it will do for now.</td>
</tr>
<tr class="even">
<td>cab</td>
<td></td>
<td>Satellite angles per pixel. Four layer file containing the satellite
and sun azimuth and zenith angles for every pixel in the image. Angles
are in radians (times 100.0). Layers are named, the order is (satAz,
satZen, sunAz, sunZen).</td>
</tr>
<tr class="odd">
<td>cac</td>
<td></td>
<td>Incidence, exitance and relative azimuth angles for every pixel.
Angles are in radians (times 100.0). The incidence angle is the angle
between the sun and the normal to the surface terrain. The exitance
angle is the angle between the satellite and the normal to the surface
terrain. The relative azimuth is the angle in the plane of the surface
terrain, between the projections of the lines to sun and satellite.</td>
</tr>
<tr class="even">
<td>cad</td>
<td></td>
<td>Re-calibrated radiance, after calibrations by ACRES/GA have been
undone and calibrations by NASA CPF have been applied. Scaling is stored
as DNscale objects.</td>
</tr>
<tr class="odd">
<td>cae</td>
<td></td>
<td>Sky view factor for every pixel. Factor is in range [0,1] (stored
scaled with DNscale object), and repesents the proportion of the sky
hemisphere visible to each pixel</td>
</tr>
<tr class="even">
<td>caf</td>
<td></td>
<td>Fiddled DEM (with slope and aspect). This has been smudged as per
James Shepherd's method for improving the estimate of aspect in areas of
sharp change. We do not currently use this, but I have reserved this
stage code for it, in case we ever do.</td>
</tr>
<tr class="odd">
<td>cag</td>
<td></td>
<td>Ortho-rectified ACRES imagery. Raw imagery was supplied by ACRES/GA,
rectification was done by Suzanne Furby at CSIRO, as a part of the
national project to compare radiometric approaches. Otherwise equivalent
to the ca2/ca3 stages.</td>
</tr>
<tr class="even">
<td>cb1</td>
<td>ba7</td>
<td>Old level of radiometric correction. See <a
href="/rsc/data_prep/landsat_scaling">landsat_scaling</a> page for
info.</td>
</tr>
<tr class="odd">
<td>cb2</td>
<td>ba8</td>
<td>ba7 clipped to standard scene extents</td>
</tr>
<tr class="even">
<td>cb3</td>
<td>ba9</td>
<td>Current (2002 version) radiometric correction. See <a
href="/rsc/data_prep/landsat_scaling">landsat_scaling</a> page for
info.</td>
</tr>
<tr class="odd">
<td>cb4</td>
<td>baa</td>
<td>ba9/cb3, but clipped to standard scene extents</td>
</tr>
<tr class="even">
<td>cb5</td>
<td></td>
<td>Used experimentally for the Charters Towers timeseries - same as cb3
but with a standard extent.</td>
</tr>
<tr class="odd">
<td>cb6</td>
<td></td>
<td>Surface reflectance calculated by 6S, using ozone climatology,
surface humidity and fixed AOD=0.05. Part of the 2010 version
radiometric processing. Currently we do not save this.</td>
</tr>
<tr class="even">
<td>cb7</td>
<td></td>
<td>Direct irradiance calculated by 6S, using the DEM elevation for each
pixel. Part of the 2010 version radiometric processing.</td>
</tr>
<tr class="odd">
<td>cb8</td>
<td></td>
<td>Experimental, topographically corrected, top-of-atmosphere
reflectance, with Danaher brdf adjustment. Topographic correction is on
direct irradiance only, diffuse irradiance is not taken into
account.</td>
</tr>
<tr class="even">
<td>cb9</td>
<td></td>
<td>Diffuse irradiance calculated by 6S, using DEM elevation for each
pixel. Part of the 2010 version radiometric processing.</td>
</tr>
<tr class="odd">
<td>cba</td>
<td></td>
<td>Surface-leaving radiance, inferred from 6S outputs of surface
reflectance and direct and diffuse irradiance. Part of the 2010 version
radiometric processing.</td>
</tr>
<tr class="even">
<td>cbb</td>
<td></td>
<td>Direct irradiance, adjusted for terrain slope. Part of the 2010
version radiometric processing.</td>
</tr>
<tr class="odd">
<td>cbc</td>
<td></td>
<td>Diffuse irradiance, adjusted for terrain &amp; sky view factor. Part
of the 2010 version radiometric processing.</td>
</tr>
<tr class="even">
<td>cbd</td>
<td></td>
<td>As for cb8, but clipped to SLATS standard footprints/extents</td>
</tr>
<tr class="odd">
<td>cbe</td>
<td></td>
<td>Merge code layer for merged SLC-off images. Encodes which file was
used in the merged product, on a per-pixel basis. This is a thematic
layer, with coding shown in the raster attribute table. Note that the
"date" field of the filename will be a day range. Merge is done by
simple infilling of missing pixels from other dates in the range.</td>
</tr>
<tr class="even">
<td>cbf</td>
<td></td>
<td>Standardized surface reflectance, adjusted to standard sun and view
angles, but without removing topography. Part of the 2010 version
radiometric processing.</td>
</tr>
<tr class="odd">
<td>cbg</td>
<td></td>
<td>Standardized surface reflectance, adjusted to standard sun and view
angles for a horizontal surface, i.e. the topography has been
"flattened". Part of the 2010 version radiometric processing.</td>
</tr>
<tr class="even">
<td>cbh</td>
<td></td>
<td>Thermal brightness temperature, in Kelvin. Calibration is using
latest NASA coefficients, and then converted from radiance to Kelvin
using a single standard equation T = K2/log(K1/L+1). Only exists for
"th" product files.</td>
</tr>
<tr class="odd">
<td>cbi</td>
<td></td>
<td>This is exactly equivalent to a cb3 file, i.e. angle-adjusted
top-of-atmosphere reflectance, using the Danaher-Walthall angle
adjusted. However, the original data was sourced from the USGS Landsat
archive, and then mosaiced and resampled to match the SLATS scene
footprints (i.e. half-scene offset and 25m pixels). This is intended as
a stop-gap measure, to allow continuity of the SLATS scene footprints
while using the USGS datasource. Scripts which accept cb3 should also
accept this.</td>
</tr>
<tr class="even">
<td>cbj</td>
<td></td>
<td>Clipped, angle-adjusted top-of-atmosphere reflectance. Exactly
equivalent to the cb4, but from the cbi, which has been derived
originally from USGS sourced imagery. Only intended for SLATS clearing
detection, in cases when the ACRES version of this date already exists,
but is incompatible with the USGS-based second date.</td>
</tr>
<tr class="odd">
<td>cbk</td>
<td></td>
<td>Topographically corrected version of cbj. For use only with SLATS
clearing, when early date is also from USGS source, so as not to
conflict with ACRES-derived cb8. This is not currently saved to
filestore.</td>
</tr>
<tr class="even">
<td>cbl</td>
<td></td>
<td>Clipped cbk. For use only with SLATS clearing, when early date is
also from USGS source, so as not to conflict with ACRES-derived cbd.
This is not currently saved to the filestore.</td>
</tr>
<tr class="odd">
<td>cbm</td>
<td></td>
<td>Top of atmosphere (at-sensor) reflectance. No other corrections are
done. Simply computed as
<code>p = pi * L * d^2 / (E * cos(theta))</code> where L is at-sensor
radiance, d is the earth-sun distance, E is the exoatmospheric
irradiance adjusted for the spectral response function of the sensor,
and theta is the solar zenith angle. This product is not currently saved
to filestore, but a script does exist to compute these:
toareflectance.py</td>
</tr>
<tr class="even">
<td>cbo-cbz</td>
<td></td>
<td>reserved experimental BRDF and atmospherically corrected stages</td>
</tr>
<tr class="odd">
<td></td>
<td>bj*</td>
<td>ACRES ortho-rectified Level 10 product</td>
</tr>
<tr class="even">
<td>** Single Date FPC**</td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td>bc2</td>
<td>FPC - Based on ba7 radiometric stage - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bc8</td>
<td>FPC - Based on ba9 radiometric stage - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bc9</td>
<td>FPC - Based on ba9 radiometric stage - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bh0</td>
<td>Masked bc2 FPC image, based on the bc2 stage (uses bc2, bd1, bd2,
bd3, bgm, bg4) - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhj</td>
<td>Masked bc8 FPC image as at 0405 stage masks, but with bd4&amp;5
(uses bc8, bd1, bd4, bd5, bg4, bgm) - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhk</td>
<td>FPC - Based on ba9 radiometric stage (uses bc9, bd1, bd4, bd5, bg4,
bgm) - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhx</td>
<td>FPC - Based on ba9 radiometric stage but doesn’t calculate FPC for
values in the ba9 that equal 255. Derived from Version 3.4 of the FPC
index, calibrated using multiple regression between transformed Landsat
DN’s and stand-scale allometric FPC calculated using the SLATS field BA
measurements (uses bc9, bd6, bd4, bd5, bg4, bgm) - OBSOLETE</td>
</tr>
<tr class="even">
<td>cc1</td>
<td>bc9</td>
<td>MLR FPC index; ignoring zeros in input bands; Landsat-5 model
applied to all images - OBSOLETE</td>
</tr>
<tr class="odd">
<td>cc2</td>
<td></td>
<td>MLR FPC index; ignoring zeros in input bands; Correct model applied
to all images</td>
</tr>
<tr class="even">
<td>cc3</td>
<td></td>
<td>RFR FPC index; ignoring zeros in input bands</td>
</tr>
<tr class="odd">
<td>cc4</td>
<td></td>
<td>MLR FPC index, as for cc2, but using topographically corrected
reflectances. Topographic correction is our simple cut-through version
of the WAK-based topographic correction.</td>
</tr>
<tr class="even">
<td>cc5</td>
<td></td>
<td>MLR FPC index, as for cc4, but using inputs derived from USGS. This
is only for use with SLATS clearing, when early date is from USGS, so as
not to conflict with ACRES-derived cc2. This is not currently saved to
the filestore.</td>
</tr>
<tr class="odd">
<td>ch1</td>
<td>bhk/bhx</td>
<td>Masked cc1 FPC image - OBSOLETE</td>
</tr>
<tr class="even">
<td>ch2</td>
<td>bhk</td>
<td>Masked bc9/cc2 FPC image - correct model applied to Landsat7</td>
</tr>
<tr class="odd">
<td>ch3</td>
<td></td>
<td>Masked FPC image, exactly as per ch2, but using the topographically
corrected input cc4</td>
</tr>
<tr class="even">
<td>ch4</td>
<td></td>
<td>Masked FPC image, as per ch2, but omitting cloud masks. This is
NEVER to be saved to the filestore, and is used only temporarily by the
timeserieschange script. It can be either the topo-corrected version or
not, as required. Repeat, this is NEVER saved, and NOT used
elsewhere</td>
</tr>
<tr class="odd">
<td>ch5</td>
<td></td>
<td>Mask FPC image, using topographically flattened input, from data
sourced from USGS instead of ACRES. Exact equivalent of the ch3, used
only for the timeserieschange script. This only intended to be used for
the SLATS clearing, and only for those cases when the original
ACRES-based file is incompatible with the USGS-based second date.</td>
</tr>
<tr class="even">
<td><strong>Water/Topographic Shadow</strong></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td>bd1</td>
<td>Water Mask (values &lt; = 68 water)</td>
</tr>
<tr class="even">
<td></td>
<td>bd2</td>
<td>Topographic shadow mask (&lt; = 5 shadow (suspect)</td>
</tr>
<tr class="odd">
<td></td>
<td>bd3</td>
<td>Topographic shadow mask II (&gt; 85 shadow) (suspect)</td>
</tr>
<tr class="even">
<td>cd1</td>
<td>bd6</td>
<td>Current (2002 version) water mask (really the water index) (values ≤
68 means water)</td>
</tr>
<tr class="odd">
<td>cd2</td>
<td>bd4</td>
<td>Current (2002 version) topographic shadow mask (really the incidence
angle in degrees) (incidence &gt; 85 means shadow)</td>
</tr>
<tr class="even">
<td>cd3</td>
<td>bd5</td>
<td>Current (2002 version) topographic shadow mask II (this is a cast
shadow index) (values ≤ 5 mean shadow)</td>
</tr>
<tr class="odd">
<td>cd4</td>
<td></td>
<td>masked water mask with topographic errors and bad data removed</td>
</tr>
<tr class="even">
<td>cd5</td>
<td></td>
<td>count of water prevalence, relating to woody extent and FPC product
eras, i.e. 8807</td>
</tr>
<tr class="odd">
<td>cd6</td>
<td></td>
<td>Water mask. As for cd1, but using topographically corrected
reflectances, as per cb8.</td>
</tr>
<tr class="even">
<td>cd7</td>
<td></td>
<td>Count of water prevalence, using cd6 as input</td>
</tr>
<tr class="odd">
<td>cd8</td>
<td></td>
<td>Extent of "permanent" water (i.e. where count &gt; 1)</td>
</tr>
<tr class="even">
<td>cd9</td>
<td></td>
<td>Water mask count over a date range, for use within the cda
calculation</td>
</tr>
<tr class="odd">
<td>cda</td>
<td></td>
<td>Water mask based on a timeseries. Count of water prevalence (from
cd1), combined with a region grow.</td>
</tr>
<tr class="even">
<td>cdb</td>
<td></td>
<td>Cast shadow mask. Zero is not in shadow, 1 is in shadow. Part of the
2010 version radiometric processing.</td>
</tr>
<tr class="odd">
<td>cdc</td>
<td></td>
<td>Incidence/exitance angle angle mask - zero is OK, 1 means sun or
view angle was too great. Part of the 2010 version radiometric
processing.</td>
</tr>
<tr class="even">
<td>ce*</td>
<td>bw1</td>
<td>Clipped binary water mask individual date</td>
</tr>
<tr class="odd">
<td>ce*</td>
<td>bw2</td>
<td>Water mask showing number of years a particular area has contained
water</td>
</tr>
<tr class="even">
<td>ce*</td>
<td>bw3</td>
<td>Binary water mask showing extent of waterbodies over time</td>
</tr>
<tr class="odd">
<td><strong>Cloud/Cloud Shadow</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>cg2</td>
<td>bg2</td>
<td>Intermediate cloud mask stage</td>
</tr>
<tr class="odd">
<td>cg3</td>
<td>bg3</td>
<td>Pre-edited cloud mask</td>
</tr>
<tr class="even">
<td>cg4</td>
<td>bg4</td>
<td>Cloud mask</td>
</tr>
<tr class="odd">
<td>cg5</td>
<td>bg5</td>
<td>par file</td>
</tr>
<tr class="even">
<td>cg6</td>
<td>bgj</td>
<td>intermediate shadow</td>
</tr>
<tr class="odd">
<td>cg7</td>
<td>bgk</td>
<td>intermediate shadow</td>
</tr>
<tr class="even">
<td>cg8</td>
<td>bgl</td>
<td>pre-edited cloud shadow mask</td>
</tr>
<tr class="odd">
<td>cg9</td>
<td>bgm</td>
<td>final Cloud Shadow Mask</td>
</tr>
<tr class="even">
<td>cga</td>
<td></td>
<td>Cloud mask calculated using dense time series of band4 and band5.
This stage was inadvertently double-booked, because someone didn't enter
it in here, and so it is also used for a temporary stage during manual
cloud masking. However, these are never saved, so we live with the
contradiction.</td>
</tr>
<tr class="odd">
<td>cgb</td>
<td></td>
<td>Cloud shadow mask derived using dense time series of dark object
methods. This stage was inadvertently double-booked, because someone
didn't enter it in here, and so it is also used for a temporary stage
during manual cloud masking. However, these are never saved, so we live
with the contradiction.</td>
</tr>
<tr class="even">
<td>cgc</td>
<td></td>
<td>Experimental. Cloud mask produced by dense time series, using
Robert's "stuff"</td>
</tr>
<tr class="odd">
<td>cge</td>
<td></td>
<td>Cloud mask image produced using Adrian Fisher's image morphology
scripts. Includes a point shapefile with the same stage name, of the
seed points used. Also optionally includes a shapefile with the option
field _poly, for areas of wispy cloud not picked up elsewhere, which
also contributes to the raster mask.</td>
</tr>
<tr class="even">
<td>cgf</td>
<td></td>
<td>Cloud shadow mask image produced using Adrian Fisher's image
morphology scripts. Also includes a point shapefile of the seed markers
used.</td>
</tr>
<tr class="odd">
<td>cgg</td>
<td></td>
<td>Cloud mask produced by stitching together two cloud masks from the
automated processing used for USGS Landsat imagery. Original source
could be any one of the available stages, e.g. dgr, dgk, etc. It is
implicitly assumed that the input images all used the same coding as the
dgr.</td>
</tr>
<tr class="even">
<td>cgh</td>
<td></td>
<td>Cloud shadow mask produced by stitching together two cloud shadow
masks from the automated processing used for USGS Landsat imagery.
Original source could be any one of the available stages, e.g. dgs, dgn,
etc. It is implicitly assumed that all the input images used the same
coding as the dgs.</td>
</tr>
<tr class="odd">
<td>cgr</td>
<td></td>
<td>Fmask-based cloud mask. See Zhu &amp; Woodcock, Rem.Sens.Env.
118(2012) pp83-94.</td>
</tr>
<tr class="even">
<td>cgs</td>
<td></td>
<td>Fmask-based cloud shadow mask</td>
</tr>
<tr class="odd">
<td>cgt</td>
<td></td>
<td>Fmask-based snow mask</td>
</tr>
<tr class="even">
<td>cgv</td>
<td></td>
<td>Manually edited version of the cgg</td>
</tr>
<tr class="odd">
<td>cgw</td>
<td></td>
<td>Manually edited version of the cgh</td>
</tr>
<tr class="even">
<td><strong>NDVI/EVI</strong></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td>cf1</td>
<td>bf1</td>
<td>NDVI/EVI rasters. Subtract 200, then divide by 200 to get the
index</td>
</tr>
<tr class="even">
<td><strong>SLATS Change Detection/Change Rasters</strong></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td>bh*</td>
<td>0405 change rasters see <a href="/proc/e0405">e0405</a></td>
</tr>
<tr class="even">
<td></td>
<td>bh2</td>
<td>e0406 auto-generated change rasters in NSW.</td>
</tr>
<tr class="odd">
<td></td>
<td>bh3</td>
<td>e0406 change rasters for NSW aftr applying masks to the bh2.</td>
</tr>
<tr class="even">
<td></td>
<td>bh5</td>
<td>e0406 final change rasters for NSW. , after editing and checks by
third party.</td>
</tr>
<tr class="odd">
<td></td>
<td>be*</td>
<td>0304 change rasters see <a
href="http://atrax/ciss/slats2000/Slats_documentation/processing/proc_be.htm">http://atrax/ciss/slats2000/Slats_documentation/processing/proc_be.htm</a></td>
</tr>
<tr class="even">
<td></td>
<td>bk*</td>
<td>0506 change rasters see <a href="/proc/e0506">e0506</a></td>
</tr>
<tr class="odd">
<td>cl*</td>
<td>bl*</td>
<td>0607, 0708 change rasters see <a href="/proc/e0607">e0607</a></td>
</tr>
<tr class="even">
<td>cl1</td>
<td></td>
<td>Time series statistics of FPC time series, for use within time
series change</td>
</tr>
<tr class="odd">
<td>cl2</td>
<td></td>
<td>Interpretation layer from time series change</td>
</tr>
<tr class="even">
<td>cl3</td>
<td></td>
<td>Change classes from time series change</td>
</tr>
<tr class="odd">
<td>cl4</td>
<td></td>
<td>Masked change classes, as for cl3 with cloud/water/topo-shadow masks
applied</td>
</tr>
<tr class="even">
<td>cl5</td>
<td></td>
<td>Manually edited version of cl4, based on visual interpretation. This
is the final change stage in NSW for e0607 and e0708. It is also the
final stage in NSW for some scenes for e8890, e9092 and e9294 - but so
too is the cm2 stage. It is also the final stage in NSW for some scenes
for the e0809 era - but so to is the clm stage.</td>
</tr>
<tr class="odd">
<td>cl6</td>
<td></td>
<td>Manually edited version of cl5, based on field work.</td>
</tr>
<tr class="even">
<td>cl7</td>
<td></td>
<td>Independantly checked version of cl6, checked by senior
officer.</td>
</tr>
<tr class="odd">
<td>cl8</td>
<td></td>
<td>Filtered (de-speckled) version of cl7. De-speckled using ???</td>
</tr>
<tr class="even">
<td>cla, clb, clc</td>
<td>apb</td>
<td>8891 split change rasters (e8889, e8990 &amp; e9091), pre and post
edits</td>
</tr>
<tr class="odd">
<td>cli</td>
<td></td>
<td>As for cl1, but using topographically corrected inputs.</td>
</tr>
<tr class="even">
<td>clj</td>
<td></td>
<td>As for cl2, but using topographically corrected inputs.</td>
</tr>
<tr class="odd">
<td>clk</td>
<td></td>
<td>As for cl3, but using topographically corrected inputs.</td>
</tr>
<tr class="even">
<td>cll</td>
<td></td>
<td>As for cl4, but using topographically corrected inputs.</td>
</tr>
<tr class="odd">
<td>clm</td>
<td></td>
<td>Manually edited version of cll, based on visual interpretation. It
is also the final stage in NSW for some scenes for the e0809 era - but
so to is the cl5 stage.</td>
</tr>
<tr class="even">
<td>cln</td>
<td></td>
<td>Manually edited version of clm, based on field work.</td>
</tr>
<tr class="odd">
<td>clo</td>
<td></td>
<td>Independantly checked version of cln, checked by senior
officer.</td>
</tr>
<tr class="even">
<td>clp</td>
<td></td>
<td>Filtered (de-speckled) version of clo. De-speckled using ???</td>
</tr>
<tr class="odd">
<td>clq</td>
<td></td>
<td>Simple two-date clearing detection, using spectral information from
two dates only. Clearing codes have same meanings as for standard
clearing rasters.</td>
</tr>
<tr class="even">
<td>clr</td>
<td></td>
<td>Re-coded clearing raster, using simplified, standardised codes. Code
values are contained in the configuration file
$RSC_VECTORISE_CLEARING_CFG. The same stage is also used for the name of
the vectorised version of this, as the codings are the same.</td>
</tr>
<tr class="odd">
<td>cls</td>
<td></td>
<td>Count of previous clearing up until the given era. Values are the
number of clearing events in all SLATS eras prior to and including the
era of the filename. Thus, clearing in the current era has a count of 1,
clearing which has been cleared before has a count greater than 1.
Pixels which were not cleared in the current era have a count of
zero.</td>
</tr>
<tr class="even">
<td>clv</td>
<td></td>
<td>Length of era, per pixel, in number of days. This became necessary
after the e1112 era, in which the era length varies due to the SLC-off
compositing.</td>
</tr>
<tr class="odd">
<td></td>
<td>bu0</td>
<td>Change raster stack ~ Regrowth Analysis. This image contains 5
layers. A comprehensive description can be found at: <a
href="/rsc/slats/clearing_composites">clearing_composites</a></td>
</tr>
<tr class="even">
<td></td>
<td>bu1</td>
<td>A repeated clearing analysis.<br />
This is a summary of layer 2 of the bu0 image. Each layer in the bu1
image represents the number of clearing events detected since the first
reported SLATS era (1988-91) up to and including that era (+1). A value
is only reported where there is change for the current era. For example,
layer 2 in the output is the number of clearing events for two
eras:<br />
1988-91 and 1991-1995.<br />
If it is an e8808 image (13 change eras in total) then layer 13 contains
the number of clearing events detected from the 1988-91 change to the
07-08 change. A description of layer 2 of the bu0 raster can be found at
<a href="/rsc/slats/clearing_composites">clearing_composites</a></td>
</tr>
<tr class="odd">
<td>cm1</td>
<td></td>
<td>Auto-generated change detection raster in NSW for e9496, e9698,
e9800, e0002, and e0204</td>
</tr>
<tr class="even">
<td>cm2</td>
<td></td>
<td>This is the final change stage in NSW for e9496, e9698, e9800,
e0002, and e0204. It is also the final stage in NSW for some scenes for
e8890, e9092 and e9294 - but so too is the cl5 stage.</td>
</tr>
<tr class="odd">
<td><strong>Timeseries FPC Product</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>ch0</td>
<td></td>
<td>Manual crop mask edits in addition to QLUMP for timeseries FPC
product</td>
</tr>
<tr class="odd">
<td></td>
<td>bhl</td>
<td>FPC - Output of the robust regression (gdaltimeseries). Seven bands
of regression statistics. Associated .dat file is the list of files used
in the regression, and the .par file is the parameters and thresholds
used to generate the time series product - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhm</td>
<td>FPC - Reset Regression output. Six bands of regression statistics. -
OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhn</td>
<td>Interpretation image. Three band image of primary thresholds from
robust regression calculations (minimum, normalized std error, switch) -
OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bho</td>
<td>Woody mask with plantation and simple topographic woody/non-woody
decision - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhp</td>
<td>CodeLayer file for chd stage - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhq</td>
<td>Woody mask incorporating multidate watermask and fill in process
corrections - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhr</td>
<td>CodeLayer file for chf(bhq) stage (see bhl.tbl file for all codes) -
OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhs</td>
<td>Woody mask also incorporating the regrowthcorrections -
OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bht</td>
<td>CodeLayer file for chh(bhs) stage (see bhl.tbl file for all codes) -
OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhu</td>
<td>Woody mask also filling in areas still null where a clear date
exists (see bhv for dates) - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhv</td>
<td>CodeLayer file for bhu stage (see bhl.tbl file for all codes) -
OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bhy</td>
<td>Clipped woody mask - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bhz</td>
<td>Clipped code layer - OBSOLETE</td>
</tr>
<tr class="even">
<td>cha</td>
<td>bhl</td>
<td>FPC - Output of the robust regression (gdaltimeseries). Seven bands
of regression statistics. Associated .dat file is the list of files used
in the regression, and the .par file is the parameters and thresholds
used to generate the time series product</td>
</tr>
<tr class="odd">
<td>chb</td>
<td>bhm</td>
<td>FPC - Reset Regression output. Six bands of regression
statistics.</td>
</tr>
<tr class="even">
<td>chc</td>
<td>bhn</td>
<td>Interpretation image. Three band image of primary thresholds from
robust regression calculations (minimum, normalized std error,
switch)</td>
</tr>
<tr class="odd">
<td>chd</td>
<td>bho</td>
<td>Woody mask with plantation and simple topographic woody/non-woody
decision</td>
</tr>
<tr class="even">
<td>che</td>
<td>bhp</td>
<td>CodeLayer file for chd stage</td>
</tr>
<tr class="odd">
<td>chf</td>
<td>bhq</td>
<td>Woody mask incorporating multidate watermask and fill in process
corrections</td>
</tr>
<tr class="even">
<td>chg</td>
<td>bhr</td>
<td>CodeLayer file for chf stage (see cha.tbl file for all codes)</td>
</tr>
<tr class="odd">
<td>chh</td>
<td>bhy</td>
<td>Woody mask also incorporating the regrowth corrections</td>
</tr>
<tr class="even">
<td>chi</td>
<td>bhz</td>
<td>CodeLayer file for chh stage (see cha.tbl file for all codes)</td>
</tr>
<tr class="odd">
<td>chj</td>
<td>na</td>
<td>Place holder for woody extent derived from code layer (chi)</td>
</tr>
<tr class="even">
<td>chm</td>
<td></td>
<td>Same as cha, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="odd">
<td>chn</td>
<td></td>
<td>Same as chb, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="even">
<td>cho</td>
<td></td>
<td>Same as chc, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="odd">
<td>chp</td>
<td></td>
<td>Same as chd, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="even">
<td>chq</td>
<td></td>
<td>Same as che, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="odd">
<td>chr</td>
<td></td>
<td>Same as chf, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="even">
<td>chs</td>
<td></td>
<td>Same as chg, but using topographically corrected reflectance inputs
(cb8)</td>
</tr>
<tr class="odd">
<td>cht</td>
<td></td>
<td>Same as chh, but using topographically corrected reflectance inputs
(cb8) <strong>This is the product supplied to clients</strong></td>
</tr>
<tr class="even">
<td>chu</td>
<td></td>
<td>Same as chi, but using topographically corrected reflectance inputs
(cb8) <strong>This is the product supplied to clients</strong></td>
</tr>
<tr class="odd">
<td>chu</td>
<td>na</td>
<td>place holder for woody extent derived from chu</td>
</tr>
<tr class="even">
<td></td>
<td>bm1</td>
<td>Regrowth corrected woody mask - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bm2</td>
<td>Regrowth corrected code layer - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bm3</td>
<td>Crop mask woody mask (if applied) - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bm4</td>
<td>Crop mask code layer (if applied) - OBSOLETE</td>
</tr>
<tr class="even">
<td></td>
<td>bm5</td>
<td>Final recode of the woody mask after crop edits, if not edits was a
copy of the bm3 - OBSOLETE</td>
</tr>
<tr class="odd">
<td></td>
<td>bm6</td>
<td>Final recode of the code layer after crop edits, if not edits was a
copy of the bm4 - OBSOLETE</td>
</tr>
<tr class="even">
<td><strong>Ground Cover/Bare Ground</strong></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td>bi0</td>
<td>Ground Cover product v1 (2005)</td>
</tr>
<tr class="even">
<td></td>
<td>bi1</td>
<td>bi0 Ground Cover with processing masks</td>
</tr>
<tr class="odd">
<td></td>
<td>bi2</td>
<td>bi1 Ground Cover with FPC &gt; 20% masked out</td>
</tr>
<tr class="even">
<td>ci1</td>
<td>bi3</td>
<td>the individual date bare ground index product v2 (2006)See <a
href="/rsc/vegetation/groundcover/stages#ci1_stage">Groundcover ci1
doco</a></td>
</tr>
<tr class="odd">
<td>ci2</td>
<td>bi4</td>
<td>bi3/ci1 bare ground index with the cloud, cloud shadow, water and
topographic shadow masks applied to the index and with areas with the
timeseries FPC greater than 15% masked out. A standard FPC image is
currently used for all images, regardless of acquisition year. This is
the timeseries FPC product for the 8806 stage chh. See <a
href="/rsc/vegetation/groundcover/stages#ci2_stage">Groundcover ci2
doco</a></td>
</tr>
<tr class="even">
<td>ci3</td>
<td>bi5</td>
<td>basic time series statistics for bare ground images: Mean, Maximum,
Minimum, Standard deviation, Median and number of observations. See <a
href="/rsc/vegetation/groundcover/stages#ci3_stage">Groundcover ci3
doco</a></td>
</tr>
<tr class="odd">
<td>ci4</td>
<td>bi6</td>
<td>basic trend analysis of bare ground index: Mean, Standard deviation,
Standard error, Slope, Intercept and number of observations. See <a
href="/rsc/vegetation/groundcover/stages#ci4_stage">Groundcover ci4
doco</a>. Timeseries slope and intercept are now reported for ground
cover in the new ci8 stage.</td>
</tr>
<tr class="even">
<td>ci5</td>
<td>bi8</td>
<td>Robust estimates of ground cover time series statistics. See <a
href="/rsc/vegetation/groundcover/stages#ci5_stage">Groundcover ci5
doco</a></td>
</tr>
<tr class="odd">
<td>ci6</td>
<td></td>
<td>Fractional cover estimates modeled using Peter Scarth's spectral
unmixing approach. Derived from cb3. Five layers for bare, green,
non-green, model residual, sum of fractions. See <a
href="/rsc/vegetation/fractional_cover">fractional_cover</a></td>
</tr>
<tr class="even">
<td>ci7</td>
<td></td>
<td>Fractional cover estimates modeled using Peter Scarth's spectral
unmixing approach. Derived from cb8. Five layers for bare, green,
non-green, model residual, sum of fractions. See <a
href="/rsc/vegetation/fractional_cover">fractional_cover</a></td>
</tr>
<tr class="odd">
<td>ci8</td>
<td></td>
<td>trend in ground cover from regression analysis of the specified
Ground Cover image time sereies. The output image is: band 1 the
regression slope; band 2 upper slope confidence interval; band 3 lower
slope confidence interval, band 4 intercept and band 5 number of
observations from a virtual raster time series of images. The slope is
scaled to an annual measure.See <a
href="/rsc/vegetation/groundcover/stages#ci8_stage">Groundcover ci8
doco</a></td>
</tr>
<tr class="even">
<td>cia</td>
<td></td>
<td>This is what Gary Bastin calls the dynamic reference pixel method.
Essentially, consider a really big window (typically 1501 pixels
square), find all the locations that are in the 90-95 percentile of the
first band of the ci5 image. Now, get the ci2 values at these locations
and find their mean value. Details of the process can be found in the <a
href="/rsc/vegetation/groundcover/acris">acris notes</a></td>
</tr>
<tr class="odd">
<td>cib</td>
<td></td>
<td>Relative ground cover index from single date (or single year
mosaic), normalized to time series mean and standard deviation. This
expresses instantaneous ground cover relative to the history of that
pixel. Normalization is to standard normal curve (i.e. subtract mean,
divide by stddev), and then scaled by multiplying by 100.</td>
</tr>
<tr class="even">
<td>cic</td>
<td></td>
<td>Ground cover index, calculated from cb3 reflectance. Note this is in
contrast to the ci1, which is bare ground.</td>
</tr>
<tr class="odd">
<td>cid</td>
<td></td>
<td>Seasonal groundcover index. The 'when' field is a month date range
m&lt;yyyymm&gt;&lt;yyyymm&gt;, indicating the first and last months of
the season in question.</td>
</tr>
<tr class="even">
<td>cie</td>
<td></td>
<td>Bare ground fraction from di7, but with masks applied for cloud,
cloud shadow and water, and also FPC&gt;15%. This is the notional
equivalent of the ci2 from the old bare ground index.</td>
</tr>
<tr class="odd">
<td>cif</td>
<td></td>
<td>Robust estimates of ground cover time series statistics, equivalent
to the ci5 stage, but derived from the cie, i.e. bare fraction.
Equivalent in all other respects.</td>
</tr>
<tr class="even">
<td>cig</td>
<td></td>
<td>Basic time series statistics for masked bare fraction images: Mean,
Maximum, Minimum, Standard deviation, Median and number of observations.
This is equivalent to the ci3 stage, but using the cie as inputs, i.e.
the bare fraction from the fractional cover algorithm.</td>
</tr>
<tr class="odd">
<td>cih</td>
<td></td>
<td>Trend in ground cover from regression analysis of the specified bare
fraction image time series. This is equivalent to the ci8, but using the
cie masked bare fraction images as inputs. It is intended for uses which
require some compatability with the old ci8.</td>
</tr>
<tr class="even">
<td>cii</td>
<td></td>
<td>delta gc images for specified years created as difference of cia -
ci2 + 100. Values greater 100 are where the ci2 value was greater than
the cia, not common, values less than 100 are where ci2 less than cia -
more common.</td>
</tr>
<tr class="odd">
<td><strong>Web Imagery</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>cj1</td>
<td>bs7</td>
<td>2 standard deviation stretch of bands 5,4,2 for web use</td>
</tr>
<tr class="odd">
<td>cj3</td>
<td>bs9</td>
<td>2 standard deviation stretch of bands 5,4,2 calculated where not
masked for web use. JPEG2000 and JPEG thumbnail also available</td>
</tr>
<tr class="even">
<td>ck</td>
<td>bt*</td>
<td>experimental automated cloudmask stage</td>
</tr>
<tr class="odd">
<td><strong>Gullies</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>cn?</td>
<td></td>
<td>Gullies. Experimental stages, to be replaced with other stages when
operational.</td>
</tr>
<tr class="odd">
<td><strong>Dense Timeseries Work</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>co?</td>
<td></td>
<td>Nic and Lisa's dense timeseries work - clouds and fire scars.
Experimental stages, to be replaced with other stages when
operational.</td>
</tr>
<tr class="odd">
<td><strong>Weed Mapping</strong></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>cp?</td>
<td></td>
<td>Jasmine's weeds mapping stages. Reserved, and many stages allocated,
but at the moment I don't have full info on these.</td>
</tr>
<tr class="odd">
<td>cp1</td>
<td></td>
<td>Prickly acacia classification. Done using maximum likelihood
supervised classification.</td>
</tr>
<tr class="even">
<td>cp2</td>
<td></td>
<td>Recoded prickly acacia classification. 1=prickly acacia, 2=other
veg, 3=groundcover/misc.</td>
</tr>
<tr class="odd">
<td>cp3</td>
<td></td>
<td>3x3 median filtered cp2, to de-speckle.</td>
</tr>
<tr class="even">
<td>cp4</td>
<td></td>
<td>As for cp3, but with dams and bores vegetation added in as separate
classes.</td>
</tr>
</tbody>
</table>


### USGS Landsat Stage Codes


The "d" stream of stage codes is intended to cover anything which is on
the USGS baseline, which differs significantly from our "c" baseline
(not least because it has a 30m pixel size).

{{ read_csv('docs/tables_import/usgs_landsat_stage_codes_tbl.csv') }}


### Mixed Satellite / Mixed Sensor Product stages


|     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|-----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ab0 | Erroneous, first attempt at 2018 Woody Veg Age Since Disturbed on 10m grid. Derived from SLATS clearing history and regrowth event prediction from heuristic classification of time series of Landsat CRF woody probabilities and clipped to the 2018 woody extent. Originally published but since replaced. Used in 2018-19 SLATS report summaries, which have been since updated in 2019-20+ open data tables (not 2018-19 tables or published report). Pixel values: 0=nonwoody; 1-254=Number of years since end of predicted regrowth event or woody period following disturbance (transition to probable nonwoody), 255=no data. Note that Clearing in Time Since Cleared includes Thinning and Partial Clearing codes. Disturbance may be due to clearing and other anthropogenic activity, fire, natural tree death, natural disaster. The age estimation may reflect a partial/thinning clearing or fire event that did not result in a transition to completely nonwoody state. |
| ab1 | Current Woody Age Since Disturbed estimation on 10m grid. As for ab0 but based on improved CRF woody model regrowth detection heuristics. TSC, now only includes time since full clearing. Not clipped to the woody extent (to satisfy reporting reqts for summarising new regrowth). Used in reporting for SLATS periods 2019-20+. Pixel values: 0-254=Number of years since last significant disturbance to the canopy, 255=no data. |
| ab2 | Age Since Disturbed code layer. Records the source data used to estimate age in the ab1/ab4 age rasters: 1=age derived from start of CRF regrowth event and no SLATS clearing, 2=age from start of CRF regrowth event following SLATS clearing, 3=age from SLATS clearing and no CRF regrowth event, 4=age from SLATS clearing - CRF regrowth/woody period straddling slats clearing event (woody model does not predict a transition), 5=age from SLATS clearing - older CRF regrowth event ending prior to current year, 6=indeterminate age: no SLATS clearing and older CRF regrowth event ending prior to current year, 7=indeterminate age: no SLATS clearing and no CRF regrowth over time series; 8-reserved for nonwoody if clipped to woody extent; 11-15=incremented from previous age where previous code was 1-5 respectively.
| ab3 | ab1/ab4 age clipped to the woody extent. Annual published woody age since disturbance. Pixel values: 0=non-woody, 1-254=Number of years since last disturbance, 255=no data.  |
| ab4 | Age Since Disturbed estimated as in ab1 but age is incremented from valid age of the previous year where no current SLATS change (and not previous indeterminate). |
| lu0 | Landuse, state-wide mosaic that combines (“current authoritative information” + “model predictions”) for a year of interest: layer 1 = most-probable landuse class; layer 2 = probability of layer 1; layer 3 = information source.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| lu1 | Landuse, state-wide mosaic that combines (“current authoritative information” + “most-recent manual mapping”) for a year of interest: layer 1 = landuse class; layer 2 = information source; layer 3 = year of information.                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| lu2 | Landuse, state-wide mosaic of (“lu1” + “limited edits based on lu0”): layer 1 = landuse class; layer 2 = information source; layer 3 = year of information. |


### SPOT stages


|     |                                                                                                                           |
|-----|---------------------------------------------------------------------------------------------------------------------------|
| aa0 | unrectified digital number. Digital number is scaled top-of-atmosphere radiance. The scaling factors are in the .dim file |
| aa0  | unrectified digital number. Digital number is scaled top-of-atmosphere radiance. The scaling factors are in the .dim file                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ba0  | Satellite angles per pixel. Four layer file containing the satellite and sun azimuth and zenith angles for every pixel in the image. Angles are in radians. Layers are named, the order is (satAz, satZen, sunAz, sunZen).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ba1  | Incidence, exitance and relative azimuth angles for every pixel. Angles are in radians. The incidence angle is the angle between the sun and the normal to the surface terrain. The exitance angle is the angle between the satellite and the normal to the surface terrain. The relative azimuth is the angle in the plane of the surface terrain, between the projections of the lines to sun and satellite.                                                                                                                                                                                                                                                                                                                                      |
| ba2  | rectified image to an unknown baseline. Digital number is scaled top-of-atmosphere radiance. As delivered by GeoImage/Terranean/A chinese company. Be wary of order of bands in the file. For GeoImage the band order is green, red, nir, swir. For Terranean and Chinese corrected imagery the band order is nir,red,green,swir. However only a few files have been checked and you MUST check them for yourself. For some GeoImage data the gains and offsets are provided in the metadata of each band and retrieving TOA radiance is obtained easily by using the DNscaling python module. Note that the null value of the TOA radiance data is 10000. Really you should be using the ba3 stage, as these problems have all been taken care of. |
| ba3  | Rectified image, as per the ba2 stage, but with the band order corrected where necessary, and the DNscale objects attached. Band ordering is green,red,nir,swir, i.e. SPOT 1,2,3,4, regardless of who rectified it for us.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| baa  | Sky view factor, calculated on the 10m grid used for rectified SPOT, from the 30m SRTM. Probably should have a different stage if using a different source DEM, but we shall see                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bab  | A DEM file extracted from a source DEM, including three layers: elevation, slope and aspect. Same stage is used regardless of which source DEM is used, and we rely on the embedded history to be sure. This stage is generally not saved, and I have provided it merely so that I can specify which source DEM to use, and then use this as input to subsequent stages. In fact, the main reason is because Tim in NSW DECCW has a higher resolution DEM he wants to use for SPOT processing.                                                                                                                                                                                                                                                      |
| bac  | Rectification baseline image, panchromatic. These are on the SPOT NSW standard scene extents, buffered by 20 km.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bad  | Rectification baseline image, 10 m red band. These are on the SPOT NSW standard scene extents, buffered by 20 km.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bb0  | bottom of atmosphere reflectance, transformed using 6S with a fixed AOD=0.05. Derived from ba3 file. The scaling factors are stored in DNscaling information.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| bb1  | bottom of atmosphere radiance (surface leaving), transformed using 6S with a fixed AOD=0.05. Derived from ba3 file. The scaling factors are stored in DNscaling information.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bb2  | At-sensor (i.e. top-of-atmosphere) reflectance                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| bb3  | Direct irradiance calculated by 6S, using the DEM elevation for each pixel. Part of the 2010 version radiometric processing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bb4  | Diffuse irradiance calculated by 6S, using DEM elevation for each pixel. Part of the 2010 version radiometric processing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bb6  | Terrain-adjusted direct irradiance                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bb7  | Terrain-adjusted diffuse irradiance, including sky-view correction                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bbf  | Surface reflectance, without topographic correction                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| bbg  | Surface reflectance, with topgraphic correction                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| bbh  | Defined for NSW OEH. This is the bbg with all masks applied, for use in subsequent FPC calculations.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| bbi  | Pan-sharpened version of the bbf to 5m, using pansharp_ltse.py.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| bbj  | Pan-sharpened version of the bbg to 5m, using pansharp_ltse.py.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| bbk  | An adaptive-histogram-stretched version of the bbm, using irs_clahe.py.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| bbl  | Pan-sharpened version of the bbg to 2.5m, using pansharp_ltse.py.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bbm  | Pan-sharpened version of the bbf to 2.5m, using pansharp_ltse.py.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bbn  | Synthetic natural colour image, with blue/green/red bands (in that order). Algorithm for synthetic blue is an empirical relationship fitted from corresponding Landsat bands. Note that this stage was previously assigned to some rubbish algorithm for the same thing, but we never actually usd it.                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| bbo  | SPOT5 super-resolution image. Created by Mike Day and Adam Roff in the Vegetation Mapping team in NSW OEH. It's a contrast enhancement technique that used spot images from 2008 to 2013, so the when field is y20082013, it's produced on topographic mapsheets.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bbp  | A part of the SPOT5 super-resolution image processing method. Created by Mike Day and Adam Roff in the Vegetation Mapping team. This particular part of the method is simply an average of a time series of bbm images, from 2008 to 2013, so the when field is y20082013. It was produced on 100k topographic mapsheets. Masks were applied to the input images. The bands and band ordering is Red, NIR, Green, SWIR.                                                                                                                                                                                                                                                                                                                             |
| bbq  | Not sure if we'll need this. Reserved for part of the SPOT5 super-resolution image processing method. Created by Mike Day and Adam Roff in the Vegetation Mapping team at OEH. This particular part of the method is an average of a time series of high-pass filtered bbm images, the same images used to create the corresponding bbo stage.                                                                                                                                                                                                                                                                                                                                                                                                      |
| bbr  | Reserved for the final SPOT5 super-resolution image. The algorithm is S=T+H/2, where S is the superresolution image, T is an average of reflectance images, bbp stage, and H is an average of the high-pass filtered reflectance images (either bbq stage or we'll compute them on the fly).                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bbs  | Defined as a composite of SPOT5 images clipped to the minimal footprints of the Sentinel-2 tiles, derived from a bbi. These are inputs to SLATS in the era of Sentinel-2 change.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bbt  | Defined as a composite of SPOT5 images clipped to the minimal footprints of the Sentinel-2 tiles, derived from a bbj. These are inputs to SLATS in the era of Sentinel-2 change.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bbu  | Pointer images to accompany the bbs and bbt files. Each pixel value identifies the image from which the corresponding pixel in the bbs/bbt came from. The image name is stored in the raster attribute table                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bbv  | Defined as a composite of SPOT6 and SPOT7 images clipped to the minimal footprints of the Sentinel-2 tiles, derived from a ba2. These were used in SLATS e1517 analysis to visually split change into e1516 and e1617.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| bbw  | Pointer images to accompany the bbv files. Each pixel value identifies the image from which the corresponding pixel in the bbv came from. The image name is stored in the raster attribute table                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bc0  | Represents either a blank cloud mask for cloud-free images produced using cloudfree.py OR cloud masks produced by an iterative procedure using Adrian Fisher's morphology scripts. The script makes an initial estimate of cloud, shape files containing additional seeds are manually created, these seeds are passed back into the script to grow more clouds, and so on. Masks are only saved to the filestore after the last iteration - i.e. the mask is complete.                                                                                                                                                                                                                                                                             |
| bc2  | Represents either a blank cloud-shadow mask for cloud-free images produced using cloudfree.py OR cloud-shadow masks produced using the same iterative procedure described for the bc0 product.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| bc3  | Cloud mask produced by Adrian Fisher's morphology scripts. Unlike bc0, no iteration is performed. However, these are an intermediate stage only and require manual editing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bc4  | Cloud mask produced by manually edited bc3.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bc5  | Cloud shadow mask produced by Adrian Fisher's morphology scripts. Unlike bc2, no iteration is performed. However, these are an intermediate stage only and require manual editing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bc6  | Cloud mask produced by manually editing bc5.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bcf  | FPC index computed from multiple linear regression model. Derived from bbf.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bcg  | FPC index computed from multiple linear regression model. Derived from bbg. This is the version of FPC which should generally be used, the others are for experimental use only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bci  | FPC index computed from multiple linear regression model. Derived from bbi.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bcj  | FPC index compured from multiple linear regression model. Derived from bbj.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bck  | Part of the spot woody FPC mapping. Composited FPC scene or mosaic derived from pan-sharpened reflectance (bbi and bbj). The composites are derived from mainly bbj pixels, but bbi pixels where there is high incidence angle or cast-shadow. The mosaics are created from a minimum fpc composite of all scenes within a standard footprint.                                                                                                                                                                                                                                                                                                                                                                                                      |
| bcl  | Part of the spot woody FPC mapping. Composited red-reflectance scene or mosaic derived from pan-sharpened reflectance (bbi and bbj). The composites are derived from mainly bbj pixels, but bbi pixels where there is high incidence angle or cast-shadow. The mosaics are created in the same way as the bck, where minimum fpc is used as the decision criteria for selection of a pixel from all scenes within a 250k topographic mapsheet. Not saved, but used as part of the processing chain.                                                                                                                                                                                                                                                 |
| bcm  | Part of the spot woody FPC mapping. Pointer images that identify the source reflectance image for each bck, bcl and ultimately bcp images.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| bcn  | Part of the spot woody FPC mapping. Time series statistics of FPC. Derived from an inter-annual time series of bck images. See fpc_spot_tsstats.py for more details. 4 bands are output: number of observations, predicted fpc, standard error, number of topographically-corrected observations.                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bco  | Part of the spot woody FPC mapping. Time series statistics of red reflectance. Derived from an inter-annual time series of bcl images. See fpc_spot_tsstats.py for more details. 4 bands are output: number of observations, predicted reflectance, standard error. number of topographically-corrected observations. These are not saved.                                                                                                                                                                                                                                                                                                                                                                                                          |
| bcp  | Part of the spot woody FPC mapping. Woody probability layer. Each pixel shows the probability of being woody. 100=0% probability, 200=100% probability. These are saved.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| bcq  | Part of the spot woody extent mapping. This is version 0.1 of the woody extent. It is derived by applying a simple threshold on the probability, bcp, image. The threshold value is stored in the image header. 0 is non-woody, 1 is woody, no data is 255.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bcr  | Part of the spot woody extent mapping. This is the final version of the woody extent. It is derived by manual editing of the bcq layer. 0 is non-woody, 1 is woody, no data is 255.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| bcs  | Spot fpc and extent. Created by combining the fpc information in layer 1 of the bcn product, with the extent information in the bcr product.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bct  | Spot woody fpc quality, which describes the quality of the woody fpc estimate in the bcs product.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| bcu  | Reserved. Part of the spot woody extent mapping. This is the final version of the woody extent. It tidies the no data values in the bcr product by using information from the landsat water body, ddh, layer.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| bcv  | Reserved. Part of the spot woody extent mapping. This is the final version of the woody extent and FPC. It tidies the no data values in the bcs product by using information from the landsat water body, ddh, layer.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| bcw  | Spot woody extent for 2012/13 or 2013/14 or 2014/15. This was created by removing woody pixels from the bcr layer using cleared pixels in the SLATS rasters. The change eras used for each woody extent year were: e0910, e1011, e1112, e1213 for 2012/13; plus e1314 for 2013/14; plus e1415 for 2014/15. Pixels mapped as loss due to fire were ignored, so they're still mapped as woody; this was done for the BCA mapping purposes.                                                                                                                                                                                                                                                                                                            |
| bcx  | This is a bcw that has been degraded onto the USGS Landsat grid. For the 30m pixel to be classified as woody, the percent of spot pixels within the Landsat pixel must be greater than a user-specified threshold. Because this threshold might vary, we don't actually save this stage to the filestore. But we might fix the threshold in the future, so we reserve this stage code for it. The script is fpc_spot_degrade.py                                                                                                                                                                                                                                                                                                                     |
| bcy  | Derived from the bcw, this is a 'refined' woody extent image, where the refinements defined in the BCA aak or aav product have been applied to either add or remove woody vegetation.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| bcz  | Derived from the bcw, this is a 'refined' woody extent image, where the refinements defined in the BCA aak or aav product have been applied to either add or native woody veg or remove exotic woody vegetation.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| bd1  | SPOT water index file. Calculated using Adrian Fisher's linear discriminant analysis index. Normal usage has that a value of the index less than or equal to 68 is probably water.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bd2  | NDVI, computed from bbg. Currently not saved.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| bd3  | NDVI, computed from bbj. Currently not saved.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| bd4  | spot multi-temporal water count, derived from bd1, on the standard scene footprints.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| bdb  | Cast shadow mask. Zero is not in shadow, 1 is in shadow. Part of the 2010 version radiometric processing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bdc  | Incidence/exitance angle angle mask - zero is OK, 1 means sun or view angle was too great. Part of the 2010 version radiometric processing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bdd  | Camphor Laurel Index - generated from a random forest model based on field training and validation data and 2 input SPOT5 images (pre and post-spring foliage flush, i.e. feb - oct). Bootstrapped to account for spatial variation in the models confidence of predicting camphor vs non-camphor and non-woody. Masked such that 1 represents a \>85% probability of being camphor and all else is no data.                                                                                                                                                                                                                                                                                                                                        |
| bg0  | Unedited cloud mask from spectral angles. Normally we don't save this unedited mask. Value\>=2 means cloud.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bg1  | Edited cloud mask, derived from bg0. Value\>=2 means cloud.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bg2  | Unedited cloud shadow, derived from correlation matching from clouds in bg1 file. Normally we don't save this unedited mask. Value\>=2 means cloud shadow.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| bg3  | Edited cloud shadow mask, derived from bg2. Value\>=2 means cloud shadow.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bhh  | Cross-calibrated FPC estimate, based on bb0. As used by NSW DECC. Currently Qld DERM are not using this.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| bhi  | Cross-calibrated FPC estimate, based on bbg reflectance, as used by NSW DECCW. Currently Qld DERM are not using this.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| bl\* | Change detection stages, for NSW DECC change detection processing. Details to be confirmed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bl0  | e0809 era of change. Change Index based on the SPOT cross calibration. Used for SPOT change analysis in the 2008/09 period                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| bl1  | e0809 era of change. Change Index with manual thresholds applied to each scene (from bl0) (Manual thresholds applied - based on areas of known woody change in the image)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bl2  | e0809 era of change. Validated change including ERDAS generated .aoi for the files                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bl3  | e0809 era of change. Changes made to the to the BL2 as a result of the quality assurance process and recoding of the possible coded areas. (Approximately 20% of the scenes were reviewed as part of the QA process for the 2008/09 Spot Change Analysis)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bl4  | e0809 era of change. Final stage used for the State-wide Change and Statistics. bl3 becomes bl4 and by default if bl3 does not exists then bl2 becomes bl4 for that particular scene                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| bl5  | e0910 era of change. Thematic layer of clearing classes, derived from automated clearing index. Clearing index is calculated on reflectances from two dates. Coded levels 30-39 are for different clearing probabilities, and code 10 means probably not clearing. All relevant masks have been applied, and coded to values greater than 100. (See also, optional components)                                                                                                                                                                                                                                                                                                                                                                      |
| bl6  | Manually edited SPOT change. Derived from bl5 or bl9. (i.e. for every bl6 there is either a bl5 or bl9 saved on the filestore. See also bl7                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| bl7  | Changes made to the to the bl6 as a result of the quality assurance process and recoding of the possible coded areas. QA is only performed on a subset of the images. The implication is that the highest quality change product is the bl7, but it only exists for a subset of scenes. Use the bl7 if it exists, otherwise use the bl6. The exception is e0910 for which no bl7 exists; the equivalent product is the blb.                                                                                                                                                                                                                                                                                                                         |
| bl8  | The raw clearing index, as output by the qv_spotclearing.py script. Can be useful for interpreting other details of change events. The values are offset, so a DN of 128 correponds to an index value of zero.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| bl9  | Thematic layer of clearing classes derived from 3-date change process. e.g. for era e1011, three input images are 2009, 2010, 2011. The earliest image is used to refine the change. Developed in NSW. Output from spotclearing_multidate.py                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bla  | Recode of the bl6 images for the e0910 era using spotclearing_multidate.py. This was a special case where the bl6 images had been created prior to the development of the multi-date change process.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| blb  | Derived from the bla as part of a quality assurance process. Pixels that were incorrectly recoded in the production of the bla were coded back to their original values. There may not be a blb for every bla. So, if a blb exists use this in preference to a bla, otherwise use the bla. Do not use the bl6 for e0910.                                                                                                                                                                                                                                                                                                                                                                                                                            |
| blc  | Regrowth mapping, using the clearing index. This is the reversed equivalent of the bl5, i.e. thresholded values of the index, coded to a preliminary classification. I am not sure whether this is useful for the current pilot project, and may not actually produce these, but I have reserved the stage code in case.                                                                                                                                                                                                                                                                                                                                                                                                                            |
| bld  | Regrowth mapping, using the clearing index. This is the raw clearing index, calculated with the start and end dates reversed, so that the main range of the clearing index is responding to increases in the amount of trees, i.e. regrowth. This is the reversed equivalent of the bl8 stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ble  | Regrowth mapping using two pairs of dates, i,e, 3 dates in all. This is similar in spirit to the bl9, but for regrowth, i.e. reversed date order.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| blf  | Regrowth mapping, using multiple dates of the bld. Not sure exactly what this will entail, but probably something like fitting a trend line through a sequence of values of the reversed clearing index, to detect when the regrowth signal is getting stronger, indicating that it is more likely to be regrowth than just noise. Yet to be determined, but this stage code reserved for it.                                                                                                                                                                                                                                                                                                                                                       |
| blr  | Re-coded clearing raster, using simplified, standardised codes. Code values are contained in the configuration file $RSC_VECTORISE_CLEARING_CFG. This is equivalent to the Landsat dlr stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| bm0  | Geoimage-supplied, pan-sharpened, pseudo natural-colour , colour-balanced, pan-sharpened ecw mosaics. Whew! These are used by NSW.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bm1  | Airbus (Astrium) supplied pan-sharpened, pseudo natural-colour , colour-balanced, pan-sharpened ecw mosaics. Whew!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| bm2  | 5 m pan-sharpened, pseudo natural-colour (using qv_spotnaturalcolour.py), colour-balanced in Mosaic Pro, pan-sharpened mosaics. This was done by NSW.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| bm3  | Airbus-supplied via (Geospatial intelligence) spot 6/7 mix, pan-sharpened, colour-balanced mosaic. (NSW)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| bm4  | Zipfiles containing the source 100k mapsheets created from bm3 supplied by Geospatial Intelligence. Each zipfile contains the 100k geotif file and associated prj, tfw and xml file with metadata. Similar to bm5 the date component is nominal. Refer to bm5 for more information about nominal date. (NSW)                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| bm5  | Similar data content to bm3 (spot 6/7) but the extent is clipped to 100k mapsheets. The date component of the name is a nominal date (eg n20190622) which represents the fact that most of the mapsheets are captured within a range of dates (some on single dates) and the range is recorded in the header of the geotif files. The date used in the name is the earlier date of the range and in case of single date mapsheets the only date. This stage code is introduced so that we will have consistent naming pattern and avoid having some mapsheets with date range and some with single dates. The single date or the date range is added to the file header. (NSW)                                                                      |
| bw0  | SPOT5 standard footprints with 10 m pixel size. Based on the footprints in the spot_nswgrs database table.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| bw1  | SPOT5 standard footprints with 5 m pixel size. Based on the footprints in the spot_nswgrs database table.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| bw2  | SPOT5 standard footprints with 10 m pixel size and clipped to state boundaries - boundary has 1.5 km buffer. Based on the footprints in the spot_nswgrs database table.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| bw3  | SPOT5 standard footprints with 5 m pixel size and clipped to state boundaries - boundary has 1.5 km buffer. Based on the footprints in the spot_nswgrs database table.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| bw4  | SPOT5 standard footprints with 10 m pixel size and clipped to NSW topographic mapsheet boundaries - either 100k or 250k.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| bw5  | SPOT5 standard footprints with 5 m pixel size and clipped to NSW topographic mapsheet boundaries - either 100k or 250k.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| bw6  | SPOT5 standard footprints with 10 m pixel size and clipped to topographic mapsheet boundaries with a 50m buffer - either 100k or 250k.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| bw7  | SPOT5 standard footprints with 5 m pixel size and clipped to topographic mapsheet boundaries with a 50m buffer - either 100k or 250k.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

### SPOTMaps stages


|     |                                                                                                                                                                            |
|-----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | SPOT Maps raw files. These are from SPOT/Image, and are a synthetic set of 3 bands, created from the original 4 SPOT bands. They should be displayed as (R,G,B) = (1,2,3). |



### Sentinel-2 Stages


{{ read_csv('docs/tables_import/sentinel_2_stages_tbl.csv') }}


### Earth-i stages

{{ read_csv('docs/tables_import/earthi_stages_tbl.csv') }}


### RapidEye stages


Note: RapidEye Tiles from Planet can be clipped on download and will
have optional field 'cRegion' where Region is the LongLat of Centroid as
in Spot5

|     |                                                                                                                            |
|-----|----------------------------------------------------------------------------------------------------------------------------|
| aa0 | Reserved for Level 1B.                                                                                                     |
| aa1 | Reserved for Level 1B orthorectified using whatever method we decide is good.                                              |
| ba1 | Orthorectified (as by RapidEye) Level 3A data.                                                                             |
| ba2 | Satellite angles onto flat terrain                                                                                         |
| ba3 | Reserved for sat angles onto sloped terrain                                                                                |
| ba4 | Orthorectified Tile Product Level 3A - 3 Band Visual.                                                                      |
| bb0 | At-sensor reflectance                                                                                                      |
| bb1 | Surface reflectance calculated using atmoscorrectrapideye.py. Currently not saved.                                         |
| bm0 | Mosaic created from Level 3A data (See ba1). Used in NSW for mosaics created by AAM Hatch via Saskia Hayes National Parks. |


### PlanetScope Processing Stages



Note: Planetscope Tiles can be clipped on download and will have
optional field 'cCentroid' where Centroid is the LongLat of Centroid as
in Spot5 - see Location. Location_id will be the tiles row and column,
using the same grid as RapidEye.

Further Planet Information:<br>
* https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:filename_codes#planetscope_processing_stages

| Stage | Description                                                                                                                                                                                                                                                                                                                                                                                  |
|-------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0   | Level 1B - PlanetScope Basic Scene Product - Scaled Top of Atmosphere Radiance (at sensor) and sensor corrected product. The Basic Scene product is designed for users with advanced image processing and geometric correction capabilities. This product has scene based framing and is not projected to a cartographic projection. Radiometric and sensor corrections applied to the data. |
| aa1   | Orthorectified aa0 using whatever method we decide is good.                                                                                                                                                                                                                                                                                                                                  |
| aa2   | This is a multi-resolution tile product, that Planet supplied to OEH for the August 2017 basemap.                                                                                                                                                                                                                                                                                            |
| ba1   | Level 3A - PlanetScope Ortho Tile Product - Radiometric and sensor corrections applied to the data. Imagery is orthorectified and projected to a UTM projection. Suitable for visual applications (3 Band). May also be a clipped product.                                                                                                                                                   |
| ba2   | Level 3A - PlanetScope Ortho Tile Product - Radiometric and sensor corrections applied to the data. Imagery is orthorectified and projected to a UTM projection. Suitable for analytical applications (4 Band). May also be a clipped product.                                                                                                                                               |
| ba3   | Level 3B - PlanetScope Ortho Scene Product - Orthorectified, scaled Top of Atmosphere Radiance (at sensor) image product suitable for visual applications (3 Band). This product has scene based framing and projected to a cartographic projection. May also be a clipped product.                                                                                                          |
| ba4   | Level 3B - PlanetScope Ortho Scene Product - Orthorectified, scaled Top of Atmosphere Radiance (at sensor) image product suitable for analytical applications (4 Band). This product has scene based framing and projected to a cartographic projection. May also be a clipped product.                                                                                                      |
| bb1   | Surface reflectance of ba1                                                                                                                                                                                                                                                                                                                                                                   |
| bb2   | Surface reflectance of ba2                                                                                                                                                                                                                                                                                                                                                                   |
| bb3   | Surface reflectance of ba3                                                                                                                                                                                                                                                                                                                                                                   |
| bb4   | Surface reflectance of ba4                                                                                                                                                                                                                                                                                                                                                                   |
| bc1   | Unusable data mask of ba1                                                                                                                                                                                                                                                                                                                                                                    |
| bc2   | Unusable data mask of ba2                                                                                                                                                                                                                                                                                                                                                                    |
| bc3   | Unusable data mask of ba3                                                                                                                                                                                                                                                                                                                                                                    |
| bc4   | Unusable data mask of ba4                                                                                                                                                                                                                                                                                                                                                                    |



### GeoEye


|     |                                                                                        |
|-----|----------------------------------------------------------------------------------------|
| aa1 | Level 1b, orthorectified, as supplied by vendors                                       |
| aa2 | Re-rectified from supplied imagery, to our own baseline (what would this be.... ?....) |



### IKONOS


|     |                                                            |
|-----|------------------------------------------------------------|
| ro2 | Calibrated radiance (we think), orthorectified by supplier |


### Quickbird data


I am not entirely happy with the current stage naming for Quickbird. If
you are trying to name any more, please see meta_info project admin and we should
discuss.

|     |                                                                                                                                                                                                                                                                                                                           |
|-----|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Sinclair Knight Mertz (SKM) delivered imagery                                                                                                                                                                                                                                                                             |
| aa1 | Sinclair Knight Mertz (SKM) delivered imagery exported to ECW by NR&M                                                                                                                                                                                                                                                     |
| dg1 | Obsolete, DO NOT USE. (dg - short for Digitalglobe) - Digitalglobe Basic Imagery                                                                                                                                                                                                                                          |
| dg2 | Obsolete, DO NOT USE. Digitalglobe Standard Imagery                                                                                                                                                                                                                                                                       |
| dg3 | Obsolete, DO NOT USE. Digitalglobe Ortho Imagery                                                                                                                                                                                                                                                                          |
| bp0 | Rubber vine classification. Imagine supervised classification using the maximum likelihood parametric rule. Training areas were defined from GPS points and polygons of observed rubber vine collected over two field trips.                                                                                              |
| bp1 | Rubber vine classification. Definiens (eCognition) classification was produced following a multiresolution segmentation calculated using a scale factor of 15. Areas of known rubber vine, observed in the field, were used to develop the process tree, along with observations on shape and density of the rubber vine. |



### MISR

#### MISR Radiance product


|     |                                                                                    |
|-----|------------------------------------------------------------------------------------|
| aa0 | Metadata                                                                           |
| aa1 | Physical radiance                                                                  |
| aa2 | TOA BRF                                                                            |
| aa3 | Surface BRF                                                                        |
| ba1 | RPV p0 (Layer 1), HG (Layer 2) and k (Layer 3) coefficients                        |
| ba2 | RPV absolute (Layer 1) and relative (Layer 2) RMSE                                 |
| ba3 | RPV predicted surface BRF                                                          |
| bb1 | Ross-Thick Li-Sparse-R iso (Layer 1), vol (Layer 2) and geo (Layer 3) coefficients |
| bb2 | Ross-Thick Li-Sparse-R absolute (Layer 1) and relative (Layer 2) RMSE              |
| bb3 | Ross-Thick Li-Sparse-R predicted surface BRF                                       |



#### MISR Surface product



|     |              |
|-----|--------------|
| aa0 | Metadata     |
| aa1 | Surface BRF  |
| aa2 | Surface HDRF |



#### MISR Geometric parameters


|     |                                                         |
|-----|---------------------------------------------------------|
| aa0 | Metadata                                                |
| aa1 | Solar zenith angle (17600m)                             |
| aa2 | Solar azimuth angle (17600m)                            |
| aa3 | View zenith angle (17600m)                              |
| aa4 | View azimuth angle (17600m)                             |
| aa5 | Relative azimuth angle (17600m)                         |
| ab1 | Bilinear interpolation of solar zenith angle (275m)     |
| ab2 | Bilinear interpolation of solar azimuth angle (275m)    |
| ab3 | Bilinear interpolation of view zenith angle (275m)      |
| ab4 | Bilinear interpolation of view azimuth angle (275m)     |
| ab5 | Bilinear interpolation of relative azimuth angle (275m) |



#### MISR Aerosol product



|     |                                                                                         |
|-----|-----------------------------------------------------------------------------------------|
| aa0 | Metadata                                                                                |
| aa1 | Regional mean AOD at 558nm                                                              |
| aa2 | Regional mean AOD at 558nm with a 3 by 3 focal median applied                           |
| ab1 | Regional best fit mixture ( Aerosol mixture with smallest chi-square fitting parameter) |
| ac1 | Regional median AOD at 558nm                                                            |



### PALSAR Processing Stages


|     |                                                                                                     |
|-----|-----------------------------------------------------------------------------------------------------|
| aa0 | Single Look Complex (SLC) L1.1 data                                                                 |
| aa1 | Multi-look intensity.                                                                               |
| aa2 | Multi-look averaged product.                                                                        |
| aa4 | Alpha product.                                                                                      |
| aa5 | Entropy product.                                                                                    |
| aa6 | Anisotropy product.                                                                                 |
| ca1 | Local incidence angle.                                                                              |
| ca2 | Pixel area correction factor.                                                                       |
| ca3 | Layover/shadow mask.                                                                                |
| ca4 | Geocoding LUT.                                                                                      |
| ca5 | Simulated SAR backscatter image in DEM geometry.                                                    |
| cb0 | Beta-0 product (not currently produced). Imagery otho-rectified to USGS Landsat pan mosaic db9.     |
| cb1 | Sigma-0 product. Imagery otho-rectified to USGS Landsat pan mosaic db9.                             |
| cb2 | Gamma-0 product (not currently produced). Imagery otho-rectified to USGS Landsat pan mosaic db9.    |
| cb3 | Sigma - 0 radiometrically corrected product. Imagery otho-rectified to USGS Landsat pan mosaic db9. |
| cb4 | Alpha product. Imagery otho-rectified to USGS Landsat pan mosaic db9.                               |
| cb5 | Entropy product. Imagery otho-rectified to USGS Landsat pan mosaic db9.                             |
| cb6 | Anisotropy product. Imagery otho-rectified to USGS Landsat pan mosaic db9.                          |
| cb7 | Gamma0 product. Imagery orthorectified to SRTM DEM (CSIRO v1.0) and radiometrically corrected.      |
| cb8 | Gamma0 residual terrain corrected product (generated using cb7).                                    |
| cb9 | Gamma0 normalized product. cb8 imagery corrected for surface moisture variations.                   |
| cd1 | Above ground biomass (AGB) derived from L-HV (cb9) and plot biomass measurements.                   |
| cd2 | Change in AGB by differencing AGB images (cd1) from two time periods.                               |
| cd? | Biophysical products (stages reserved).                                                             |



### TerraSAR-X Processing Stages


|     |                                                                                              |
|-----|----------------------------------------------------------------------------------------------|
| aa0 | Slant-range Single-look Complex (SSC). Apogee delivered imagery imported from native format. |
| aa1 | Calibrated SSC.                                                                              |
| aa2 | Multi-look intensity.                                                                        |
| ca0 | GA SRTM DEM v1.0 bilineraly interpolated to image resolution and extent.                     |
| ca1 | Local incidence angle.                                                                       |
| ca2 | Pixel area correction factor.                                                                |
| ca3 | Layover/shadow mask.                                                                         |
| ca4 | Geocoding LUT.                                                                               |
| ca5 | Simulated SAR backscatter image in DEM geometry.                                             |
| cb1 | Geocoded ground-range sigma-0.                                                               |
| cb2 | Geocoded ground-range terrain-corrected gamma-0.                                             |



### DEM Files (currently just the SRTM file)


|     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|-----|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Elevation data, 90m SRTM                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| aa1 | Slope calculated from the 90m SRTM elevation data (percentage) (probably should not be used)                                                                                                                                                                                                                                                                                                                                                                                                              |
| aa2 | Aspect calculated from 90m SRTM elevation data (degrees clockwise from north) (probably should not be used)                                                                                                                                                                                                                                                                                                                                                                                               |
| aa3 | Elevation data, 90m SRTM, as processed by CGIAR, for hole-filling, etc.                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ab0 | Elevation data, 30m SRTM, CSIRO version 0.x processing                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ac0 | Elevation data, 30m SRTM, CSIRO version 1.0 processing. Also known as the DSM.                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ac1 | Elevation data with vegetation removed (ha, ha...), CSIRO version 1.0 processing. Also known as the DEM.                                                                                                                                                                                                                                                                                                                                                                                                  |
| ac2 | A smoothed version of the DEM product, CSIRO version 1.0 processing. Also known as the DEM-S.                                                                                                                                                                                                                                                                                                                                                                                                             |
| ac3 | A hydrologically-enforced version of the DEM-S product, CSIRO version 1.0 processing. Also known as the DEM-H.                                                                                                                                                                                                                                                                                                                                                                                            |
| ac4 | Ellipsoid-geoid separation. This is not, by itself, an elevation model. Rather, this is a model of the separation between the geoid and the ellipsoid. This stage code is used with the "what" field "nanaem", mostly because I could not decide what else to call it. This stage is the model supplied by Geosciences Australia, as their AUSGeoid09 product, and allegedly includes the correction to the Australian Height Datum. See <http://www.ga.gov.au/ausgeoid/nvalcomp.jsp> for futher details. |
| ac5 | Elevation data, 30m SRTM (1 Arcsecond), USGS version 3.0 processing. Ocean pixels have a height of 0m, no data val is -32767, Horizontal datum is WGS84, vertical datumm is EGM96 Geoid                                                                                                                                                                                                                                                                                                                   |



### Airborne Hymap data



|     |                                                                                                                                   |
|-----|-----------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Reflectance - HYCORR with EFFORT refinement as delivered by HyMap                                                                 |
| aa6 | Reflectance resampled to 5m on 5m grid                                                                                            |
| aa6 | Reflectance resampled to match satellite imagery. 's' field specifies satellite. 'r' field, if present is the degraded resolution |
| ab0 | Radiance                                                                                                                          |
| ac0 | Geometry Lookup Table (GLT) used for rectification                                                                                |
| ac1 | Angles info generated from .gps file                                                                                              |
| ac6 | Angles info resampled to 5m on 5m grid                                                                                            |



### MODIS stages


Stage codes for MODIS Collection 5 were associated with each MODIS
product/version. The product is identified by the optional 'p' tag (see
below). The version number by the optional 's' tag.

For MODIS Collection 6 products, and other MODIS products obtained from
third-parties (e.g. csiro fractional cover), we will move to a more
standard approach to assigning stage codes, and they won't be tied to
product/version codes. The stage code will define the modis
product/version, and so we won't need optional fields in the filenames
either.

#### Collection 5 filenaming


**MOD09GA - 005 (Product - version)**

|     |                                                  |                                                                         |
|-----|--------------------------------------------------|-------------------------------------------------------------------------|
| aa0 | Terra daily surface reflectance 500m, bands 1-7. | Surface reflectance all bands.                                                                                                    |
| aa1 | data quality                                     | MODIS data quality. The original reference for this description is at<br>https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/surface_reflectance/daily_l2g_global_1km_and_500m/mod09ga.<br>Band 1 = MODLAND QA srcBits:<br> 0=corrected product produced at ideal quality;<br> 1=corrected product produced at less than ideal quality some or all bands;<br> 2=corrected product not produced due to cloud effects all bands;<br> 3=corrected product not produced due to other reasons some or all bands may be fill value.<br>Band 2 = Band 1 reflectance quality:<br> 0 = highest quality;<br> 8 = dead detector, data interpolated in L1B;<br> 9 = solar zenith greater than or equal to 86 degrees;<br> 10 = solar zenith greater than or equal to 85 and less than 86 degrees;<br> 11 = missing input;<br> 12 = internal constant used in place of climatological data for at least one atmospheric constant;<br> 13 = correction out of bounds pixel constrained to extreme allowable value;<br> 14 = L1B data faulty;<br> 15 = not processed due to deep ocean or clouds.<br>Bands 3-8 = reflectance quality for bands 2-7: (Same values as Band 2).<br>Band 9 = Atmospheric correction performed: 1=YES, 0=NO.<br>Band 10 = Adjacency correction performed: 1=YES, 0=NO. |
| aa2 | Angles for sun and satellite, 1000m.             | 1\. sensor zenith, 2. sensor azimuth, 3. sun zenith, 4. sun azimuth.                                                              |

**MYD09GA - 005**

|     |                                                 |                                                                      |
|-----|-------------------------------------------------|----------------------------------------------------------------------|
| aa0 | Aqua daily surface reflectance 500m, bands 1-7. | surface reflectance all bands                                        |
| aa1 | data quality                                    | Same as aa1 stage for MOD09GA - 005                                  |
| aa2 | Angles for sun and satellite, 1000m.            | 1\. sensor zenith, 2. sensor azimuth, 3. sun zenith, 4. sun azimuth. |

**MOD09GQ - 005** and **MOD09GQK - 005**

|     |                                                  |                                                                                                                          |
|-----|--------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| aa0 | Terra daily surface reflectance 250m, bands 1-2. | surface reflectance all bands                                                                                            |
| aa1 | data quality                                     | MODIS data quality. The original reference for this description is at<br>https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/surface_reflectance/daily_l2g_global_250m/mod09gq<br>Band 1 = MODLAND QA srcBits:<br>0=corrected product produced at ideal quality;<br>1=corrected product produced at less than ideal quality some or all bands;<br>2=corrected product not produced due to cloud effects all bands;<br>3=corrected product not produced due to other reasons some or all bands may be fill value.<br>Band 2 = Cloud State:<br>0 = clear;<br>1 = cloudy;<br>2 = mixed;<br>3 = not set, assumed clear.<br>Band 3 = Band 1 reflectance quality:<br>0 = highest quality;<br>8 = dead detector, data interpolated in L1B;<br>9 = solar zenith greater than or equal to 86 degrees;<br>10 = solar zenith greater than or equal to 85 and less than 86 degrees;<br>11 = missing input;<br>12 = internal constant used in place of climatological data for at least one atmospheric constant;<br>13 = correction out of bounds pixel constrained to extreme allowable value;<br>14 = L1B data faulty;<br>15 = not processed due to deep ocean or clouds.<br>Band 4 = Band 2 reflectance quality (Same values as Band 3).<br>Band 5 = Atmospheric correction performed: 1=YES, 0=NO.<br>Band 6 = Adjacency correction performed: 1=YES, 0=NO.                                                    |

**MYD09GQ - 005** and **MYD09GQK**

|     |                                                 |                                     |
|-----|-------------------------------------------------|-------------------------------------|
| aa0 | Aqua daily surface reflectance 250m, bands 1-2. | surface reflectance all bands.      |
| aa1 | data quality                                    | Same as aa1 stage for MYD09GQ - 005 |

**MOD09Q1 - 005**

|     |                                                            |                                                                                                                          |
|-----|------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| aa0 | Terra 8-day composite surface reflectance 250m, bands 1-2. | surface reflectance all bands.                                                                                           |
| aa1 | data quality                                               | MODIS data quality. The original reference for this description is at<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/surface_reflectance/daily_l2g_global_250m/mod09q1<br> Band 1 = MODLAND QA srcBits:<br> 0=corrected product produced at ideal quality;<br> 1=corrected product produced at less than ideal quality some or all bands;<br> 2=corrected product not produced due to cloud effects all bands;<br> 3=corrected product not produced due to other reasons some or all bands may be fill value.<br> Band 2 = Cloud State:<br> 0 = clear;<br> 1 = cloudy;<br> 2 = mixed;<br> 3 = not set, assumed clear.<br> Band 3 = Band 1 reflectance quality:<br> 0 = highest quality;<br> 8 = dead detector, data interpolated in L1B;<br> 9 = solar zenith greater than or equal to 86 degrees;<br> 10 = solar zenith greater than or equal to 85 and less than 86 degrees;<br> 11 = missing input;<br> 12 = internal constant used in place of climatological data for at least one atmospheric constant;<br> 13 = correction out of bounds pixel constrained to extreme allowable value;<br> 14 = L1B data faulty;<br> 15 = not processed due to deep ocean or clouds.<br> Band 4 = Band 2 reflectance quality (Same values as Band 3).<br> Band 5 = Atmospheric correction performed: 1=YES, 0=NO.<br> Band 6 = Adjacency correction performed: 1=YES, 0=NO.<br> Band 7 = Different orbit from 500 m: 1=YES, 0=NO.                                                 |

**MYD09Q1 - 005**

|     |                                                           |                                      |
|-----|-----------------------------------------------------------|--------------------------------------|
| aa0 | Aqua 8-day composite surface reflectance 250m, bands 1-2. | surface reflectance all bands.       |
| aa1 | data quality                                              | Same as aa1 stage for MOD09Q1 - 005. |

**MOD09A1 - 005**

|     |                                                           |                                      |
|-----|-----------------------------------------------------------|--------------------------------------|
|aa0| Terra 8-day composite surface reflectance 500m, bands 1-7. | surface reflectance all bands. |
|aa1| data quality | MODIS data quality. The original reference for this <br>description is at <br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/surface_reflectance/8_day_l3_global_500m/mod09a1. <br> Band 1 = MODLAND QA srcBits: <br> 0=corrected product produced at ideal quality; <br> 1=corrected product produced at less than ideal quality some or all bands; <br> 2=corrected product not produced due to cloud effects all bands; <br> 3=corrected product not produced due to other reasons some or all bands may be fill value. <br> Band 2 = Band 1 reflectance quality: <br> 0 = highest quality; <br> 8 = dead detector, data interpolated in L1B; <br> 9 = solar zenith greater than or equal to 86 degrees; <br> 10 = solar zenith greater than or equal to 85 and less than 86 degrees; <br> 11 = missing input; <br> 12 = internal constant used in place of climatological data for at least one atmospheric constant; <br> 13 = correction out of bounds pixel constrained to extreme allowable value; <br> 14 = L1B data faulty; <br> 15 = not processed due to deep ocean or clouds. <br> Bands 3-8 = reflectance quality for bands 2-7: (Same values as Band 2). <br> Band 9 = Atmospheric correction performed: 1=YES, 0=NO. <br> Band 10 = Adjacency correction performed: 1=YES, 0=NO. |
| aa2 | reflectance state flags | MODIS reflectance state flags. The original reference for this description is at<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/surface_reflectance/8_day_l3_global_500m/mod09a1.<br> Band 1 = MOD35 Cloud:<br> 0 = clear;<br> 1 = cloudy;<br> 2 = mixed;<br> 3 = not set, assumed clear.<br> Band 2 = cloud shadow: 1=YES, 2=NO.<br> Band 3 = land/water flag:<br> 0 = shallow ocean;<br> 1 = land;<br> 2 = ocean coastlines and lake shorelines;<br> 3 = shallow inland water;<br> 4 = ephemeral water;<br> 5 = deep inland water;<br> 6 = continental/moderate ocean;<br> 7 = deep ocean.<br> Band 4 = Aerosol Quantity:<br> 0 = climatology;<br> 1 = low;<br> 2 = average;<br> 3 = high.<br> Band 5 = Cirrus Detected:<br> 0 = none;<br> 1 = small;<br> 2 = average;<br> 3 = high;<br> Band 6 = internal cloud algorithm flag: 1=Cloud, 0=No Cloud.<br> Band 7 = internal fire algorithm: 1=fire, 0=no fire.<br> Band 8 = MOD35 snow/ice flag: 1=yes, 0=no.<br> Band 9 = pixel is adjacent to cloud: 1=yes, 0=no.<br> Band 10 = BRDF correction performed: 1=yes, 0=no.<br> Band 11 = Internal snow algorithm flag: 1=yes, 0=no.                                                           |

**MYD09A1 - 005**

|     |                                                           |                                      |
|-----|-----------------------------------------------------------|--------------------------------------|
| aa0 | Aqua 8-day composite surface reflectance 500m, bands 1-7. | surface reflectance all bands.       |
| aa1 | data quality                                              | same as aa1 stage for MOD09A1 - 005. |
| aa2 | reflectance state flags                                   | same as aa2 stage for MOD09A1 - 005. |

**MOD13Q1 - 005**

|     |                                                                                                                                                                                                |                                                                                                                          |
|-----|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| aa0 | Terra Vegetation Indices (NDVI and EVI) 16-days composite 250 m. along with reflectance from bands 1,2,3 and 7 and view zenith, sun zenith and relative azimuth angles, and julian day of year | MODIS Vegetation Indices. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/vegetation_indices/16_day_l3_global_250m/mod13q1.<br> Band 1 = NDVI.<br> Band 2 = EVI.<br> Band 3 = MODIS Band 1 reflectance.<br> Band 4 = MODIS Band 2 reflectance.<br> Band 5 = MODIS Band 3 reflectance.<br> Band 6 = MODIS Band 7 reflectance.<br> Band 7 = view zenith angle in degrees.<br> Band 8 = sun zenith angle in degrees.<br> Band 9 = relative azimuth angle in degrees.<br> Band 10 = Julian day of year.      |
| aa1 | VI Quality                                                                                                                                                                                     | MODIS Vegetation Indices QA. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/vegetation_indices/16_day_l3_global_250m/mod13q1.<br> Band 1 = MODLAND QA:<br> 0 = VI produced, good quality;<br> 1 = VI produced, but check other QA;<br> 2 = Pixel produced, but most probably cloudy;<br> 3 = Pixel not produced due to other reasons than clouds.<br> Band 2 = VI usefulness:<br> 0 = Highest quality;<br> 1 = Lower quality;<br> 2 = Decreasing quality;<br> 4 = Decreasing quality;<br> 8 = Decreasing quality;<br> 9 = Decreasing quality;<br> 10 = Decreasing quality;<br> 12 = Lowest quality;<br> 13 = Quality so low that it is not useful;<br> 14 = L1B data faulty;<br> 15 = Not useful for any other reason/not processed.<br> Band 3 = Aerosol quantity:<br> 0 = Climatology;<br> 1 = Low;<br> 2 = Average;<br> 3 = High.<br> Band 4 = Adjacent cloud detected:<br> 1 = Yes;<br> 0 = No.<br> Band 5 = Atmosphere BRDF correction performed:<br> 1 = Yes;<br> 0 = No.<br> Band 6 = Mixed Clouds<br> 1 = Yes;<br> 0 = No.<br> Band 7 = Land/Water Flag:<br> 0 = Shallow ocean;<br> 1 = Land (Nothing else but land);<br> 2 = Ocean coastlines and lake shorelines;<br> 3 = Shallow inland water;<br> 4 = Ephemeral water;<br> 5 = Deep inland water;<br> 6 = Moderate or continental ocean;<br> 7 = Deep ocean.<br> Band 8 = Possible snow/ice<br> 1 = Yes;<br> 0 = No.<br> Band 9 = Possible shadow<br> 1 = Yes;<br> 0 = No.                                                 |
| aa2 | A mosaic of NDVI created from the Terra (MOD13Q1) aa0 images                                                                                                                                   |                                                                                                                          |

**MOD13A1 (Terra) and MYD13A1 (Aqua) - 006**

|     |                                                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-----|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Terra Vegetation Indices (NDVI and EVI) 16-days composite 500 m. along with reflectance from bands 1,2,3 and 7 and view zenith, sun zenith and relative azimuth angles, and julian day of year | MODIS Vegetation Indices. The original reference for this description is at:<br> https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/myd13a1_v006.<br> Band 1 = NDVI.<br> Band 2 = EVI.<br> Band 3 = MODIS Band 1 reflectance.<br> Band 4 = MODIS Band 2 reflectance.<br> Band 5 = MODIS Band 3 reflectance.<br> Band 6 = MODIS Band 7 reflectance.<br> Band 7 = view zenith angle in degrees.<br> Band 8 = sun zenith angle in degrees.<br> Band 9 = relative azimuth angle in degrees.<br> Band 10 = Julian day of year.                                                                                                                                                 |
| aa1 | VI Quality                                                                                                                                                                                     | MODIS Vegetation Indices QA. The original reference for this description is at:<br> https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/myd13a1_v006.<br> Band 1 = MODLAND QA:<br> 0 = VI produced, good quality;<br> 1 = VI produced, but check other QA;<br> 2 = Pixel produced, but most probably cloudy;<br> 3 = Pixel not produced due to other reasons than clouds.<br> Band 2 = VI usefulness:<br> 0 = Highest quality;<br> 1 = Lower quality;<br> 2 = Decreasing quality;<br> 4 = Decreasing quality;<br> 8 = Decreasing quality;<br> 9 = Decreasing quality;<br> 10 = Decreasing quality;<br> 12 = Lowest quality;<br> 13 = Quality so low that it is not useful;<br> 14 = L1B data faulty;<br> 15 = Not useful for any other reason/not processed.<br> Band 3 = Aerosol quantity:<br> 0 = Climatology;<br> 1 = Low;<br> 2 = Average;<br> 3 = High.<br> Band 4 = Adjacent cloud detected:<br> 1 = Yes;<br> 0 = No.<br> Band 5 = Atmosphere BRDF correction performed:<br> 1 = Yes;<br> 0 = No.<br> Band 6 = Mixed Clouds<br> 1 = Yes;<br> 0 = No.<br> Band 7 = Land/Water Flag:<br> 0 = Shallow ocean;<br> 1 = Land (Nothing else but land);<br> 2 = Ocean coastlines and lake shorelines;<br> 3 = Shallow inland water;<br> 4 = Ephemeral water;<br> 5 = Deep inland water;<br> 6 = Moderate or continental ocean;<br> 7 = Deep ocean.<br> Band 8 = Possible snow/ice<br> 1 = Yes;<br> 0 = No.<br> Band 9 = Possible shadow<br> 1 = Yes;<br> 0 = No.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| aa2 | A composited vegetation index product, created by combining pixels from the aa0 (Terra) and aa1 (Aqua) vegetation index products.                                                              | The Terra and Aqua images are generated 8-days apart; the earlier Aqua image is merged with the later Terra image. The output image consists mostly of Terra pixels, except where the quality of the Aqua image is thought to be better. The MODIS QA and usefulness ratings from the aa1 images are used to create these two rules to determine when to use Aqua: 1. The Aqua QA rating is better than Terra's for the corresponding pixels. 2. Where the Terra QA score is 1 or 2 and is the same as the Aqua QA score and the Aqua usefulness score is better than the Terra usefulness score. So we effecively choose non-cloudy pixels over cloudy pixels, followed by the most useful pixels. |
| aa3 | VI quality image corresponding to the aa2. The pixel values are the same as the aa1.                                                                                                           |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa4 | The code layer that identifies the source of each pixel in the aa2 and aa3 images.                                                                                                             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa5 | A mosaic of NDVI created from the aa2 images                                                                                                                                                   |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa6 | A mosaic of NDVI created from the Terra (MOD13A1) aa0 images                                                                                                                                   |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa7 | A mosaic of NDVI created from the Aqua (MYD13A1) aa0 images                                                                                                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

**MCD43A1 - 005**

|     |                                                                                                                                                    |                                                                                                                                     |
|-----|----------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Combined 16-Day Composite BRDF/Albedo Parameters 500 m                                                                                             | MODIS BRDF/Albedo parameters (iso, vol and geo). The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/brdf_albedo_model_parameters/16_day_l3_global_500m/mcd43a1<br> Bands 1-10 = The iso parameter for Bands 1-7 and visible, NIR and SWIR.<br> Bands 11-20 = The vol parameter for Bands 1-7 and visible, NIR and SWIR.<br> Bands 21-30 = The geo parameter for Bands 1-7 and visible, NIR and SWIR.                                                |
| aa1 | BRDF/Albedo shape indicators                                                                                                                       | MODIS BRDF/Albedo shape indicators. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/brdf_albedo_model_parameters/16_day_l3_global_500m/mcd43a1<br> Band 1 = NIR band ratio of nadir to 45deg forward scattering at 45deg sun angle.<br> Band 2 = NIR band ratio of white-sky albedo to BRDF isotropic paramater value.<br> Band 3 = normalised difference anistropy index at 45deg sun angle (NDAX).<br> Band 4 = structural scattering index SSI = ln(f_vol_nir/f_geo_red).                                                     |
| aa2 | Reserved. NBAR Reflectance calculated from the BRDF parameters with solar zenith angle at 45 degrees. Equivalent to Geoff Milne's old z45 product. |                                                                                                                                     |
| aa3 | Reserved. NDVI calculated from aa2. Equivalent to Geoff Milne's old n45 product.                                                                   |                                                                                                                                     |
| aa4 | Reserved. EVI calculated from aa2.                                                                                                                 |                                                                                                                                     |

**MCD43A2 - 005**

|     |                                                             |                                                                                                                            |
|-----|-------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| aa1 | Combined 16-Day Composite BRDF/Albedo Data Quality 500 m    | MODIS BRDF/Albedo Quality. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/brdf_albedo_quality/16_day_l3_global_500m/mcd43a2<br> Band 1 = BRDF Albedo Quality:<br> 0 = Processed, good quality (full BRDF inversion);<br> 1 = Processed, see other QA (magnitude BRDF inversions);<br> 255 = Fill Value.<br> Band 2 = Snow BRDF Albedo:<br> 0 = now-free albedo retrieved;<br> 1 = Snow albedo retrieved;<br> 255 = Fill Value.<br> Band 3 = MODIS band 1 quality<br> 0 = best quality, full inversion (WoDs, RMSE majority good);<br> 1 = good quality, full inversion;<br> 2 = Magnitude inversion (numobs >=7);<br> 3 = Magnitude inversion (numobs >=3&<7);<br> 4 = Fill value.<br> Bands 4-9 = MODIS bands 2-7 Quality. See explanation for band 3 above.                                         |
| aa2 | Combined 16-Day Composite BRDF/Albedo Ancillary Information | MODIS BRDF/Albedo Ancillary information. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/brdf_albedo_quality/16_day_l3_global_500m/mcd43a2<br> Band 1 = Platform:<br> 0 = Terra;<br> 1 = Terra/Aqua;<br> 2 = Aqua;<br> Band 2 = Land/Water classification<br> 0 = Shallow ocean;<br> 1 = Land (Nothing else but land);<br> 2 = Ocean and lake shorelines;<br> 3 = Shallow inland water;<br> 4 = Ephemeral water;<br> 5 = Deep inland water;<br> 6 = Moderate or continental ocean;<br> 7 = Deep ocean.<br> Band 3 = solar zenith angle at Local Solar Noon (Degrees).<br> Band 4 = QAFill:<br> 0 = Not Fill Value;<br> 1 = Fill Value.                                                                                              |

**MCD43A4 - 005**

|     |                                                                                              |                                                                                                                                        |
|-----|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Combined 16-Day Composite NBAR Reflectance 500 m. With sun zenith angle at local solar noon. | MODIS NBAR Reflectance,; 16-day composite. Sun zenith is local solar noon. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/nadir_brdf_adjusted_reflectance/16_day_l3_global_500m/mcd43a4<br> Surface Reflectance bands 1-7.                                                                                             |
| aa3 | Reserved. NDVI calculated from aa0. Equivalent to Geoff Milne's old nd0 product.             |                                                                                                                                        |
| aa4 | Reserved. EVI calculated from aa0.                                                           |                                                                                                                                        |

**MCD45A1 - 005**

|     |                                           |                                                                                                                                                                                                                     |
|-----|-------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0 | Burned area mapping day of year 500 m     | MODIS Burned Area Mapping. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/burned_area/monthly_l3_global_500m/mcd45a1<br> Further Documentation can be found at http://modis-fire.umd.edu/BA_usermanual.html.<br> Band 1 = Approximate Julian day of burning from eight days before the beginning of the month to eight days after the end of the month, or a code indicating unburned areas, snow, water, or lack of data:<br> 0 = unburned;<br> 1-366 = approximate Julian day of burning;<br> 900 = snow or high aerosol;<br> 9998 = water bodies (internal);<br> 9999 = water bodies (seas and oceans);<br> 10000 = not enough data to perform inversion throughout the period.                                                                                                                                     |
| aa1 | burned area mapping quality               | MODIS Burned Area Mapping Quality. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/burned_area/monthly_l3_global_500m/mcd45a1<br> Further Documentation can be found at http://modis-fire.umd.edu/BA_usermanual.html.<br> Band 1 = Pixel QA, from 1  most confident to 4 least confident:<br> 1 = most confidently detected pixels;<br> 2 = pixels where backward and forward direction in time predict the same change;<br> 3 = pixels selected in the first stage of the contextual analysis;<br> 4 = pixels selected in the second stage of the contextual analysis.<br> Band 2 = Number of observations where the temporal consistency test is passed.<br> Band 3 = Number of observations used in the temporal consistency test.<br> Band 4 = Direction in time in which burning was detected:<br> 1 = forward;<br> 2 = backward;<br> 3 = both.<br> Band 5 = water (NDVI < 0.1 and b7 < 0.04): 1 = YES; 0 = NO.<br> Band 6 = low NDVI (NDVI < 0.1): 1 = YES; 0 = NO.<br> Band 7 = shallow, ephemeral, deep inland water (QA from MOD09 = 3, 4, 5 AND NDVI <0.1): 1 = YES; 0 = NO.<br> Band 8 = cloud (from MOD09 internal cloud mask): 1 = YES; 0 = NO.<br> Band 9 = cloud shadow (from MOD09 internal cloud mask): 1 = YES; 0 = NO.<br> Band 10 = view and solar zenith angle mask (Vz > 65 threshold or Sz > 65): 1 = YES; 0 = NO.<br> Band 11 = high view and solar zenith angle (Vz > 50 and Sz > 55 ): 1 = YES; 0 = NO.<br> Band 12 = snow OR [high aerosol (from MOD09 QA) AND high view / solar zenith (Vz > 55 and Sz > 55)]: 1 = YES; 0 = NO.                                                                                     |
| aa2 | burned area mapping gap range information | MODIS Burned Area Gap Range information. The original reference for this description is at:<br> https://lpdaac.usgs.gov/lpdaac/products/modis_products_table/burned_area/monthly_l3_global_500m/mcd45a1<br> Further Documentation can be found at http://modis-fire.umd.edu/BA_usermanual.html.<br> Band 1 = Gap range 1, Julian day of the start of the gap.<br> Band 2 = Gap range 1, number of missing days including the start day.<br> Band 3 = Gap range 2, Julian day of the start of the gap.<br> Band 4 = Gap range 2, number of missing days including the start day.                                                                                                                                     |


#### Collection 6 and third-party product filenaming


|     |                                                                                                                                                                                                                                   |
|-----|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ba0 | CSIRO fractional cover version 2.2. Three layers: bare, pv, npv. pixel values range from 0-100 corresponding to percent cover. NULL=255.                                                                                          |
| ba1 | Flag layer for the CSIRO fractional cover.                                                                                                                                                                                        |
| ba2 | Monthly median fractional cover computed using ba0 products.                                                                                                                                                                      |
| ba3 | Code files for the montly median fractional cover images.                                                                                                                                                                         |
| ba4 | Gap-filled monthly median fractional cover, derived from the ba2. This is for those who don't like missing pixels.                                                                                                                |
| ba5 | Code layer for the gap-filled, ba4, product, which identifies those pixels that were gap filled and the 'resolution' of the pixel required to fill the gap.                                                                       |
| bb0 | CSIRO fractional cover version 3.0.1. Three layers: bare, pv, npv. pixel values range from 0-100 corresponding to percent cover. NULL=255. Note that there is no corresponding flag layer like there was for version 2.2          |
| bb1 | Monthly median fractional cover computed using bb0 products.                                                                                                                                                                      |
| bb2 | Code files for the montly median fractional cover images.                                                                                                                                                                         |
| bb3 | Gap-filled monthly median fractional cover, derived from the bb1. This is for those who don't like missing pixels.                                                                                                                |
| bb4 | Code layer for the gap-filled, bb3, product, which identifies those pixels that were gap filled and the 'resolution' of the pixel required to fill the gap.                                                                       |
| bb5 | 8-day fractional cover as obtained from CSIRO for version 3.1.0                                                                                                                                                                   |
| bb6 | Monthly median fractional cover as obtained from CSIRO for version 3.1.0. Created from 3 single-layer images (bare, PV, NPV).                                                                                                     |
| bb7 | Monthly median total cover as obtained from CSIRO for version 3.1.0                                                                                                                                                               |
| bb8 | Monthly median fractional cover as obtained from CSIRO for version 3.1.0. This is created from CSIRO's single 3-band image. One would think the pixel values would be identical to those obtained in the bb6, but apparently not. |
| bc0 | CSIRO fractional cover version 3.1.0 Three layers: bare, pv, npv.                                                                                                                                                                 |
| bw0 | Standard footprint for MODIS fractional cover mosaics, which identifies non-ocean pixels.                                                                                                                                         |



### NSW ADS (Large format digital camera Stages


These are used in NSW for aerial photography.

| stage code | description                                                                                                                                                                                                                                                                                                                                    |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ea0        | Level 1 RGB                                                                                                                                                                                                                                                                                                                                    |
| ea1        | Level 1 CIR                                                                                                                                                                                                                                                                                                                                    |
| ea2        | Level 1 4 bands                                                                                                                                                                                                                                                                                                                                |
| eb0        | Level 2 module RGB                                                                                                                                                                                                                                                                                                                             |
| eb1        | Level 2 module CIR                                                                                                                                                                                                                                                                                                                             |
| eb2        | Level 2 module 4 bands                                                                                                                                                                                                                                                                                                                         |
| ec0        | Level 2 ecw mosaic RGB as supplied by LPI                                                                                                                                                                                                                                                                                                      |
| ec1        | Level 2 ecw mosaic CIR as supplied by LPI                                                                                                                                                                                                                                                                                                      |
| ec2        | Level 2 ecw mosaic 4 bands as supplied by LPI                                                                                                                                                                                                                                                                                                  |
| eda        | 1m DEM                                                                                                                                                                                                                                                                                                                                         |
| edb        | 2m DEM                                                                                                                                                                                                                                                                                                                                         |
| edc        | 5m DEM                                                                                                                                                                                                                                                                                                                                         |
| edd        | 10m DEM                                                                                                                                                                                                                                                                                                                                        |
| ede        | 15m DEM                                                                                                                                                                                                                                                                                                                                        |
| edf        | 20m DEM                                                                                                                                                                                                                                                                                                                                        |
| edg        | 8m DEM (ex LPI)                                                                                                                                                                                                                                                                                                                                |
| ef0        | Level 2 tif, 100k topographic mapsheet, RGB mosaic created from eb0.                                                                                                                                                                                                                                                                           |
| ef1        | Level 2 tif, 100k topographic mapsheet, CIR mosaic created from eb1.                                                                                                                                                                                                                                                                           |
| ef2        | Level 2 tif, 100k topographic mapsheet, 4 band mosaic created from eb2.                                                                                                                                                                                                                                                                        |
| ef3        | Level 2 tif, 100k topographic mapsheet, RGB mosaic created from eb0, degraded to 250 cm.                                                                                                                                                                                                                                                       |
| ef4        | Level 2 tif, 100k topographic mapsheet, CIR mosaic created from eb1, degraded to 250 cm.                                                                                                                                                                                                                                                       |
| ef5        | Level 2 tif, 100k topographic mapsheet, 4 band mosaic created from eb2, degraded to 250 cm                                                                                                                                                                                                                                                     |
| ef6        | Mosaic of eb0 or eb2 modules, with or without colour corrections, as bands and area may vary; adequate metadata must accompany. They will use the 'other' project code ([filenaming_ads&#project_codes](/rs/data_management/filenaming_ads&#project_codes)). These are created using Desktop software, like ERDAS products - see Scott Taylor. |
| ef7        | surface reflectance derived from the ef0, by cross-calibration to Landsat surface reflectance. The band order is blue, green, red.                                                                                                                                                                                                             |
| ef8        | surface reflectance derived from the ef1, by cross-calibration to Landsat surface reflectance. The band order is green, red, NIR.                                                                                                                                                                                                              |
| ef9        | surface reflectance derived from the ef2, by cross-calibration to Landsat surface reflectance. Band order is blue, green, red, NIR.                                                                                                                                                                                                            |
| efa        | surface reflectance derived from the ef3, by cross-calibration to Landsat surface reflectance.                                                                                                                                                                                                                                                 |
| efb        | surface reflectance derived from the ef4, by cross-calibration to Landsat surface reflectance.                                                                                                                                                                                                                                                 |
| efc        | surface reflectance derived from the ef5, by cross-calibration to Landsat surface reflectance.                                                                                                                                                                                                                                                 |
| eg0        | 0.5 m pixel binary tree image, derived from the green band in one of the ef0, ef1 or ef2.                                                                                                                                                                                                                                                      |
| eg1        | 5m pixel tree percent image, derived from the eg0, resampled onto the SPOT5 pixel grid, plus some masking of water pixels.                                                                                                                                                                                                                     |
| eg2        | 5m pixel tree percent image, derived from the eg1, after masking out pixels where the SPOT tree probability is 0                                                                                                                                                                                                                               |


### NSW Historic aerial photo (film based) stages



|         |                                                                                                                                                                                                                         |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0     | raw, not rectified, photograph derived from a scanned contact print.                                                                                                                                                    |
| aa1     | raw, not rectified, photograph derived from a scanned diapositive. These are normally supplied by LPI with ECW compression.                                                                                             |
| aa2     | RESERVED raw, not rectified, photograph derived from a scanned contact print. We are reserving this stage in case we need to convert from an aa0 to a file created using the OpenJPEG driver - like we did for the aa3. |
| aa3     | raw, not rectified, photograph derived from an aa1. These are not ECW compressed, and created using the OpenJPEG driver in GDAL.                                                                                        |
| ba0     | a rectified version of an aa0. These are not ortho-rectified, e.g. they are just rubber-sheeted. Rectified to a well-defined baseline using a well-defined method. Scale may vary across the image.                     |
| ba1     | a rectified version of an aa1. These are not ortho-rectified, e.g. they are just rubber-sheeted. Rectified to a well-defined baseline using a well-defined method. Scale may vary across the image.                     |
| ba2     | a manually ortho-rectified version of an aa0, i.e. rectified using a DEM. Rectified to a well-defined baseline using a well-defined method. Scale is consistent across the image.                                       |
| ba3     | a manually ortho-rectified version of an aa1, i.e. rectified using a DEM. Rectified to a well-defined baseline using a well-defined method. Scale is consistent across the image.                                       |
| ba4     | an automatically ortho-rectified version of an aa0, i.e. rectified using a DEM. Rectified to a well-defined baseline using a well-defined method. Scale is consistent across the image.                                 |
| ba5     | an automatically ortho-rectified version of an aa1, i.e. rectified using a DEM. Rectified to a well-defined baseline using a well-defined method. Scale is consistent across the image.                                 |
| ~~ba4~~ | ~~A mosaic of rectified ba0 images.~~                                                                                                                                                                                   |
| ba5     | A mosaic of rectified ba1 images.                                                                                                                                                                                       |
| ~~ba6~~ | ~~A mosaic of ortho-rectified ba2 images.~~                                                                                                                                                                             |
| ba7     | A mosaic of ortho-rectified ba3 images.                                                                                                                                                                                 |


### Disaster Management Constellation (DMC)



| stage code | description                                               |
|------------|-----------------------------------------------------------|
| aa0        | Level L1R at-sensor radiance, unrectified                 |
| aa1        | Level L1T at-sensor radiance, ortho-rectified by supplier |



### ZY-3 (SASMAC)



| stage code | description                                                                                                                                                                   |
|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0        | SASMAC-supplied at-sensor radiance, unrectified. \<fc #FF0000\>WARNING: Calibration is known to be very poor for most of the images available.\</fc\>                         |
| aa1        | SASMAC-supplied at-sensor radiance, ortho-rectified using SASMAC-supplied Rational Polynomial Coefficients. Likely positional error is \<30m. Note calibration warning above. |
| ab0        | Top-of-atmosphere radiance, from aa1 stage. Note that mostly this is not accurate, due to the calibration issues mentioned above.                                             |



### Climate Related Products



| stage code | description                                                                                             |
|------------|---------------------------------------------------------------------------------------------------------|
| aa1        | Standardised Precipitation Index at 5km with a 20 month lag. Derived from SILO monthly rainfall rasters |



### Landuse, BCA mapping and land-management activity related products - used in NSW

These are for products with a "what" of naluma.


| Stage code | description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| aa0        | landuse vector coded using ALUM Version 7.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| aa1        | BCA overrides vector layers                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| aa2        | BCA steep slopes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|            | **The next 2 are not BCA related, but are instead multi-date change information.**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| aa3        | A summary of clearing rasters, including the revised clearing, USGS do7 stage files. It shows the most-recent clearing event, except if new change is coded in the do7 this takes precedence. The pixel codes are 1=Fire, 2=Agriculture, 3=Infrastructure, 4=Forestry, 5=Physical process damage, 6=Missed clearing in previous era.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aa4        | A summary of clearing rasters, including the revised clearing, USGS do7 stage files. It shows the most-recent clearing event, except if new change is coded in the do7 this takes precedence. The pixel codes are 2=Agriculture, 3=Infrastructure, 4=Forestry.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa5        | summary of SLATS multi-era, multi-satellite clearing rasters                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| aa6        | draft BCA map without overrides                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| aa7        | draft lineage layer, without overrides to accompany the aa6                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| aa8        | draft lineage layer vector, without overrides, created by vectorising the aa7.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aa9        | draft BCA map with overrides, with or without ADS information from previously-edited refinements                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| aaa        | draft lineage layer, with overrides to accompany the aa9                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aab        | draft lineage layer vector, with overrides, created by vectorising the aaa                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|            | **The following codes are for BCA map products incorporating ADS Tree map data**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| aac        | unedited refinements image with ADS data to accompany the aa9. This could include previously-edited refinements images.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| aad        | A partially refined BCA map with overrides, created using the aag.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| aae        | Lineage layer for the aad                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| aaf        | Lineage layer vector, created by vectorising the aae                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aag        | A partially-edited refinements image, created from the aac, and to accompany the aad                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aah        | A fully (final product) QA'd BCA map with overrides, created using the aak.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| aai        | Lineage layer for the aah                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| aaj        | Lineage layer vector, created by vectorising the aai                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aak        | A completely-edited refinements image, created from the aac or aag, and to accompany the aah                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| aal        | Grouped-up Lineage layer for the aah                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aam        | Grouped-up lineage layer vector, created by vectorising the aal.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|            | **The following codes are for BCA map products that don't have ADS Tree cover data**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| aan        | as for the aac, but created without ADS tree cover data                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| aao        | as for the aad                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aap        | as for the aae, but to accompany the aao                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aaq        | as for the aaf, but to accompany the aao                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aar        | as for the aag, but to accompany the aao                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aas        | as for the aah                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| aat        | as for the aai, but to accompany the aas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aau        | as for the aaj, but to accompany the aas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aav        | as for the aak, but to accompany the aas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aaw        | as for the aal, but to accompany the aas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aax        | as for the aam, but to accompany the aas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| aay        | This is an image that shows the difference between two **BCA maps** using bca_diff.py. We don't save these files as they're only created as needed. The image contains the pixel values from the most-recent map where they are different to the previous map. The 'd' option field shows the dates for the most recent and previous versions, respectively. The 's' option field shows the stage codes for the most recent and previous versions, respectively.                                                                                                                                                                                                                                                                                                                                                                                   |
| aaz        | This is an image that shows the difference between two **lineage** layers using bca_diff.py. We don't save these files as they're only created as needed. The image contains the pixel values from the most-recent layers where they are different to the previous layer. The 'd' option field shows the dates for the most recent and previous versions, respectively. The 's' option field shows the stage codes for the most recent and previous versions, respectively.                                                                                                                                                                                                                                                                                                                                                                        |
| ab0        | the final BCA map **with** ADS tree map data, clipped to the topographic mapsheet boundary without a buffer (created by clipping the aah). Map version 1 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ab1        | the grouped-up lineage raster, clipped to the topographic mapsheet boundary without a buffer, to accompany the ab0 (created by clipping the aal). Map version 1 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ab2        | the grouped-up lineage vector created from the ab1. Map versions 1-4 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ab3        | the final BCA map **without** ADS tree map data, clipped to the topographic mapsheet boundary without a buffer (created by clipping the aas). Map version 1 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ab4        | the grouped-up lineage raster, clipped to the topographic mapsheet boundary without a buffer, to accompany the ab3 (created by clipping the aaw). Map version 1 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ab5        | the grouped-up lineage vector created from the ab4. Map version 1 only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ab6        | Grouped-up lineage layer raster for version 2-4 of the NVR map, to accompany the aah. Grouped lineage was not created from map version 5. The original was the aal stage, but there are sufficient differences to warrant a new stage code. It contains 8 layers: 1. The 'most-sensitive' group (the notion of precedence in the first grouped-lineage layer, aal stage, was dropped); 2. The number of layers that apply for each pixel across all groups; 3. Excluded areas only (pixel value of 1); 4. The number of act sections for Group 4 (Prescribed - Sensitive); 5. The number of act sections for Group 3 (Prescribed - Vulnerable); 6. The number of act sections for Group 2 (Prescribed - Regulated); 7. The number of act sections for Group 1 (Prescribed - Unregulated); 8. Where the NVR map method was used (pixel value of 1). |
| ab7        | Grouped-up lineage layer vector for the aah. This is like the aam, but for version 2-4 of the map, it's derived from layers 3-8 in the ab6. Grouped lineage was not created from map version 5. The vector contains the attributes GroupName and NumLayers (for number of overriding layers in the group).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ab8        | Grouped-up lineage layer raster for version 2-4 of the NVR map, like the ab6, but to accompany the aas. Grouped lineage was not created from map version 5.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ab9        | as per the ab7, but to accompany the aas layers, derived from layers 3-8 in the ab8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| aba        | A layer with the category 2 - vulnerable areas shown, derived from the ab6 (map version 1-4) or aah (map version 5+). It simply shows where the layer is Vulnerable or not (1 or 0). The statewide or staged-release mosaics may have had the pyramid layers stripped out for use in the map portal.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| abb        | A layer with the category 2 - vulnerable areas shown, derived from the ab8 (map version 1-4) or aas (map version 5; this stage code was deprecated for map version 6+). It simply shows where the layer is Vulnerable or not (1 or 0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| abc        | A layer with the category 2 - sensitive areas shown, derived from the ab6 (map version 1-4) or aah (map version 5+). It simply shows where the layer is Sensitive or not (1 or 0). The statewide or staged-release mosaics may have had the pyramid layers stripped out for use in the map portal.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| abd        | A layer with the category 2 - sensitive areas shown, derived from the ab8 (map version 1-4) or aas (map version 5; this stage code was deprecated for map version 6+). It simply shows where the layer is Sensitive or not (1 or 0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| abe        | A layer with the excluded areas shown, derived from the ab6 (map version 1-4) or aah (map version 5+). It simply shows where the layer is Excluded or not (1 or 0). The statewide or staged-release mosaics may have had the pyramid layers stripped out for use in the map portal.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| abf        | A layer with the excluded areas shown, derived from the ab8 (map version 1-4) or aas (map version 5; this stage code was deprecated for map version 6+). It simply shows where the layer is Excluded or not (1 or 0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| abg        | The NVR Map, derived from the aah stage, with an extra pixel code (6) that shows where the vulnerable and sensitive layers intersect. The statewide or staged-release mosaics may have had the pyramid layers stripped out for use in the map portal. Only for map version 3 files. The 6 colours are shown for later versions in the aah.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| abh        | The NVR Map, derived from the aas stage, with an extra pixel code (6) that shows where the vulnerable and sensitive layers intersect. Only for map version 3 files. The 6 colours are shown for later versions in the aas.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| abi        | A mask that identifies where the refinements image should be edited.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| abj        | The revised transitional map - 4-colour (3 category) map derived from the aah stage. The 4 colours are vulnerable, sensitive, both vulnerable and sensitive, and excluded.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| abk        | A layer that shows the vulnerable, sensitive, and overlapping vulnerable and sensitive areas. This is effectively the transitional map. It's the same as the abj, but without the excluded layer.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| abl        | This is the NVR map, like the aah, but used for code calculations. Some overrides are given a different status in this map, compared to the nvr map.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

|               |                                                                                                                                                                                                                                                                                                |
|---------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ac stages     | **The 'ac' stages are for files created as part of a BCA (NVR) map review. See [map_revisions](/rs/statewide_analysis/bca_map/map_revisions)**                                                                                                                                                 |
| ac0           | raw zip file of data for a BCA (NVR) map review                                                                                                                                                                                                                                                |
| ac1           | The patch template file, which defines the region of the NVR map that will be revised as part of the review.                                                                                                                                                                                   |
| ac2           | The landuse patch file, containing updated landuse polygons for the review                                                                                                                                                                                                                     |
| ac3           | The overrides patch file, containing the updated overrides polygons and layers for the review, as supplied. See also aca.                                                                                                                                                                      |
| ac4           | An unedited refinement patch image                                                                                                                                                                                                                                                             |
| ac5           | An edited refinement patch image                                                                                                                                                                                                                                                               |
| ac6           | A pre-review NVR map patch image                                                                                                                                                                                                                                                               |
| ac7           | A post-review NVR map patch image                                                                                                                                                                                                                                                              |
| ac8           | A post-review lineage patch image                                                                                                                                                                                                                                                              |
| ac9           | A NVR map patch record from the bcareview_cases database table, as a vector file - not saved                                                                                                                                                                                                   |
| aca           | The overrides patch file, containing the updated overrides polygons and layers for the review, plus all the layers in the most recent overrides mapsheet. That is: the ac3 merged with the aa1.                                                                                                |
| acb           | A pre-review grouped lineage patch file. Equivalent to the ab6, but for patches. Not normally saved. Grouped lineage was not created from map version 5.                                                                                                                                       |
| acc           | A post-review grouped lineage patch file. Equivalent to the ab6, but for patches. Grouped lineage was not created from map version 5.                                                                                                                                                          |
| acd           | A steep slopes patch                                                                                                                                                                                                                                                                           |
| **ad stages** |                                                                                                                                                                                                                                                                                                |
| ad0           | Non-woody clearing geodatabase, with pixel values 1: Yes – It has been cleared after a certain time, 2: Uncertain, 3: Update – It was cleared before a certain time and landuse mapping needs to be updated in the next version. Each polygon is also attributed with the SLATS clearing code. |
| ad1           | Non-woody clearing raster at 10m pixel size, created from the ad0, where non-woody clearing definitely happened in the era (as identified in the ad0 vector). Each pixel is coded for the SLATS clearing minor code, coded in the ad0 vector.                                                  |
| ad2           | Non-woody clearing raster with ‘some pixels recoded to forestry classes were they fall inside a known area of forestry, as defined by the best available layer(s) of forestry at the time of processing.’. Derived from the ad1 and ancillary layers                                           |
| ad3           | Non-woody clearing raster with standardised codes, derived from the ad2. With 1=fire, 2=agriculture, 3=infrastructure, 4=forestry, etc                                                                                                                                                         |
| ad4           | Non-woody nett clearing raster. Created from the ad2 to remove the areas of nonwoody clearing coincident with woody clearing (aer/aet) and/or woody extent (bcy).                                                                                                                              |
| ad5           | Non-woody nett clearing raster with standardised slats codes. Created from the ad3, to remove the areas of nonwoody clearing coincident with woody clearing (aer/aet) and/or woody extent (bcy).                                                                                               |
| ad6           | Vector version of the explained layer                                                                                                                                                                                                                                                          |
| ad7           | Rasterised version of ad6                                                                                                                                                                                                                                                                      |
| ad8           | equivalent to ad1 but for non-woody clearing from the previous era                                                                                                                                                                                                                             |
| ad9           | equivalent to ad2 but for non-woody clearing from the previous era                                                                                                                                                                                                                             |
| ada           | equivalent to ad3 but for non-woody clearing from the previous era                                                                                                                                                                                                                             |
| adb           | equivalent to ad4 but for non-woody clearing from the previous era                                                                                                                                                                                                                             |
| adc           | equivalent to ad5 but for non-woody clearing from the previous era                                                                                                                                                                                                                             |
| add           | ada added to (overwrites) ad3 to give the combined complete Non Woody Disturbance for an era                                                                                                                                                                                                   |


### Assorted Non-satellite Rasters

These are generally associated with "na" satellite fields in the "what"
field.



| stage code | description                                                                                                                                                                                                                                             |
|------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ab1        | State mask, derived by rasterising the Geoscience Australia state_boundaries polygons and coding for each state. Pixel codes are: 0=OCEAN/NULL, 1=ACT, 3=NSW, 4=NT, 5=QLD, 6=SA, 7=TAS, 8=VIC, 9=WA, 10=N/A                                             |
| ab2        | Australian ocean mask, derived from ab1 raster with land buffered by 2 pixels. Pixel codes are: 0=LAND, 1=OCEAN. Please assess the suitability for your application before use. Parts of the intertidal zone have been masked as ocean in some regions. |
| ab3        | Buffered Qld land mask derived from ab1 raster with land buffered by 2 pixels. Pixel codes are: 1=buffered Qld land, 0=ocean.                                                                                                                           |



## Map Projection (pp)



The original definition of this field has exactly two characters, no
more and no less. Naturally these has turned out to be insufficient for
some purposes, so certain extensions have been defined, in
[Map Projection Extensions](#map-projection-extensions).
Apart from those, the bulk of this section refers to the two character
version of this field.

First character

{{ read_csv('docs/tables_import/map_projection_pp_tbl.csv') }}

Second character

|            |     |                                                                                                                                                                                                                                                  |
|------------|-----|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| UTM/MGA    | x   | Not on the AMG (no idea what this would mean - don't use it)                                                                                                                                                                                     |
|            | 0   | Zone 50                                                                                                                                                                                                                                          |
|            | 1   | Zone 51                                                                                                                                                                                                                                          |
|            | 2   | Zone 52                                                                                                                                                                                                                                          |
|            | 3   | Zone 53                                                                                                                                                                                                                                          |
|            | 4   | Zone 54                                                                                                                                                                                                                                          |
|            | 5   | Zone 55                                                                                                                                                                                                                                          |
|            | 6   | Zone 56                                                                                                                                                                                                                                          |
| Albers     | 0   | The SLATS-defined Albers Equal Area, which has standard parallels 12.0°S and 28.0°S, and centred at 145°E,28°S.                                                                                                                                  |
|            | 1   | The Qld govt defined Albers, used in SIR, standard parallels at 13°10’S and 25°50’S, centred on 146°E,0°S. N.B. Actually, it turns out that SIR does not use this. Rather, it was made up by Simone, for QLUMP, and is really only used by them. |
|            | 2   | Australian Albers Projection, EPSG:3577. Standard parallels are -18 and -36, central meridian is 132                                                                                                                                             |
|            | 3   | Australian Albers Projection, EPSG:9473. **GDA2020** Standard parallels are -18 and -36, central meridian is 132                                                                                                                                 |
| LCC        | 0   | NSW variant of Lambert Conformal Conic, EPSG:3308. This has standard parallels at 30°45’S and 35°45’S, central meridian 145°E, and latitude of origin 33°15’S.                                                                                   |
|            | 1   | Geoscience Australia variant of Lambert Conformal Conic, EPSG:3112. This has standard parallels at 18°S and 36°S, central meridian 134°E, and latitude of origin 0°                                                                              |
|            | 2   | reserved - GDA2020 - NSW variant of Lambert Conformal Conic, EPSG:8058. (assume the following remains true) This has standard parallels at 30°45’S and 35°45’S, central meridian 147°E, and latitude of origin 33°15’S.                          |
|            | 3   | reserved - GDA2020 - Geoscience Australia variant of Lambert Conformal Conic, EPSG:7845. (assume the following remains true) This has standard parallels at 18°S and 36°S, central meridian 134°E, and latitude of origin 0°                     |
| Sinusoidal | 0   | The MODIS standard sinusoidal projection, on the sphere.                                                                                                                                                                                         |
| Geographic | 0   | Geographic Coordinates in latitude/longitude on the WGS84 or GDA94 spheroid.                                                                                                                                                                     |
|            | 1   | Geographic Coordinates in latitude/longitude on the **GDA2020** spheroid.                                                                                                                                                                        |



### Map Projection Extensions



When two characters is insufficient, the following possible extensions
have been defined. These should be used with caution, and only when
necessary.

| Projection             | Extension                                                                                                                                                                                                                                                                                                                                                                                                                        |
|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| UTM not in zones 50-59 | Simply use both characters of the zone number. The rule is that if the field has three characters, including the 'm', then the remaining two are the two digits of the zone number. Thus, for a zone \< 10, one would need a leading zero. Note that we do not have the hemisphere as part of the filename, so the same zone in both the north and south hemispheres would have the same filename. We assume this never happens. |



## Optional Components

### Image Resolution



For many different reasons, the resolution of the image will need to be
encoded in the filename. We have now (Feb 2012) reserved the 'r' tag for
an option field encoding this. Note (below) that this was already done
in a different manner for the airborne lidar filenames, so they may need
to be treated as a special case, but for all other sensors we are now
trying to reserve the 'r' tag for this use, and defining its value as a
distance with a units specifier attached. Note that it is definitely NOT
permitted to include a "." within a field. If using the 'r' tag in this
manner, then the units string must *always* be included. Some examples
would be

| Field    | Resolution                 |
|----------|----------------------------|
| \_r5m    | 25 metres                  |
| \_r250cm | 250 centimetres, i.e. 2.5m |
| \_r30m   | 30 metres                  |
| \_r1km   | 1 kilometre                |



### Creation Date



For some products, we have chosen to record the creation date in the
filename. This is recorded using the 'c' tag for an option field, with
the date being given as a `yyyymmdd` string.



### MODIS



|       |      |                                                                                 |
|-------|------|---------------------------------------------------------------------------------|
| MODIS | oxxx | The origin of the data source. lpd (LP DAAC), lan (MODIS Lance near real time). |
|       | p    | The MODIS EOS product code (eg \_pmcd43a1).                                     |
|       | sxx  | The modis collection number (e.g. 05).                                          |

FIRST: PRODUCT VERSION COMPONENT

|            |        |                                                                              |
|------------|--------|------------------------------------------------------------------------------|
| ASTER      | \_svv1 | s version number and processing level of the raw data as ordered from the US |
|            |        | vv 2 digit version number (v.v)                                              |
|            |        | 1 represents the processing level (a = 1a, b = 1b, ..)                       |
| MISR       | \_svv  | s MISR processing version                                                    |
|            |        | vv last two digits in the HDF filename                                       |
| PALSAR     | \_ivvv | i Off-nadir angle                                                            |
|            |        | vvv 3 digit number with decimal place removed (e.g. 34.3 deg is 343)         |
| TERRASAR-X | \_mvv  | i Imaging mode                                                               |
|            |        | sc ScanSAR                                                                   |
|            |        | sm StripMap                                                                  |

SECOND: PRODUCT TYPE COMPONENT

|                   |     |                                                                                                                              |
|-------------------|-----|:-----------------------------------------------------------------------------------------------------------------------------|
| MISR              | mx  | ml local mode (275)m                                                                                                         |
|                   |     | mg global mode (1100m or 17600)                                                                                              |
| PALSAR/TERRASAR-X | pxx | p polarisation flag and xx polarisation type                                                                                 |
|                   |     | phh HH                                                                                                                       |
|                   |     | pvv VV                                                                                                                       |
|                   |     | phv HV                                                                                                                       |
|                   |     | pvh VH                                                                                                                       |
|                   |     | pdo dual co-polarised (HH and VV)                                                                                            |
|                   |     | pdx dual cross-polarised (HH and HV)                                                                                         |
|                   |     | pp1 fully polarimetric (HH, VV, HV, VH)                                                                                      |
|                   |     | N.B. HV itself is typically termed cross polarised but there is a cross polarised channel in the dual polarised combination. |

THIRD: LAYERSTACK COMPONENT (MISR)

|     |                                                                        |
|-----|:-----------------------------------------------------------------------|
| aa  | MISR Aa 26.1° view-angle aft near-nadir camera (blue, green, red, NIR) |
| af  | MISR Af 26.1° view-angle for near-nadir camera (blue, green, red, NIR) |
| an  | MISR An nadir camera (blue, green, red, NIR)                           |
| ba  | MISR Ba 45.6° view-angle aft camera (blue, green, red, NIR)            |
| bf  | MISR Bf 45.6° view-angle for camera (blue, green, red, NIR)            |
| ca  | MISR Ca 60.0° view-angle aft camera (blue, green, red, NIR)            |
| cf  | MISR Cf 60.0° view-angle for camera (blue, green, red, NIR)            |
| da  | MISR Da 70.5° view-angle aft camera (blue, green, red, NIR)            |
| df  | MISR Df 70.5° view-angle for camera (blue, green, red, NIR)            |

|     |                                                                   |
|-----|:------------------------------------------------------------------|
| s1  | Blue band (±26.1°, ±45.6°, ±60.0°, ±70.5°, nadir)                 |
| s2  | Green band (±26.1°, ±45.6°, ±60.0°, ±70.5°, nadir)                |
| s3  | Red Band (±26.1°, ±45.6°, ±60.0°, ±70.5°, nadir)                  |
| s4  | NIR band (±26.1°, ±45.6°, ±60.0°, ±70.5°, nadir)                  |
| vg  | View geometric parameters (±26.1°, ±45.6°, ±60.0°, ±70.5°, nadir) |
| xx  | Single layer product                                              |

THIRD: JAXA IDENTIFIER (PALSAR)

|        |               |                                   |
|--------|---------------|-----------------------------------|
| PALSAR | \_jvvvvvvvvvv | i JAXA orbit identifier           |
|        |               | vvvvvvvvvv 10 digit unique number |



### AIRBORNE LIDAR OPTIONAL COMPONENTS



|        |         |                                                                                                 |
|--------|---------|-------------------------------------------------------------------------------------------------|
| First  | txxxyyy | x and y numeric                                                                                 |
|        |         | This is the x and y tile (500 m by 500 m) number for lidar acquisition, e.g. t001001            |
| Second | rnnnn   | n numeric                                                                                       |
|        |         | This is the spatial resolution (m) of raster products with 0.1 scale factor, e.g. r0025 (2.5 m) |



### SPOT Optional components



For the Spot Change stages (bl\*). The location (scene centre) and
acquisition date of each image used in the change detection needs to be
recorded. The location and date of the most recent image is recorded in
the locationid and when part of the filename code. Option fields 'l' and
'w' are used to identify the location and acquisition date of the
earlier image.

|        |           |                                                                                                                                                                 |
|--------|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| First  | lggggtttt | The location (scene centre longitude, gggg, and latitude, tttt, with the leading '1' removed from the longitude) of the earliest image in the change detection. |
| Second | wyyyymmdd | The GMT date (yyyymmdd) of the earliest image used in the change detection.                                                                                     |

For example, if two images s5hgre_46933144_20081104_bbgm5.img and
s5hgre_46953144_20080410_bbgm5.img were used as inputs to the change
detection, then the output image will be
s5hgre_46933144_20081104_bl5m5_l46953144_w20080410.img.

For the spot woody vegetation extent version 0.1, the woody probability
threshold used to create this image from the bcp product is specified
with the 't' option field. This value must be in the range 0-100.

|       |     |                                                                                                                                           |
|-------|-----|-------------------------------------------------------------------------------------------------------------------------------------------|
| First | t87 | The woody probability threshold, above which vegetation is considered to be woody, used to create the bcq products from the bcp products. |



### Landuse, BCA map and land management activity optional components - NSW



|           |                                                                                                                      |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| cYYYYMMDD | The file's nominal creation date. This is used mainly for versioning the BCA mapping products. For example c20160907 |



### Planetscope and Rapideye Optional Components

| Code      | Description                                                                                                                                                                                                                                                                                                                                                   |
|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cLLLLllll | Planetscope and Rapideye images may be provided as clipped images for project based work due to the high cost of downloads. They will have the same LocationId as the source tile or scene. 'c' for 'centroid' plus the centroid expressed as LonLat. First four digits are (longitude - 100) \* 100, second 4 digits are abs(latitude) \* 100, as for Spot5. |
| lx        | This specifies the tile zoom level for the multi-resolution tile products that Planet publish on their map server - aa2 stage. 'l' mean zoom level, the 'x' is the zoom-level number. It can be 1 or 2 digits.                                                                                                                                                |

## Suffix



**.xxx** where xxx is the three letter file suffix

{{ read_csv('docs/tables_import/suffix_tbl.csv') }}


