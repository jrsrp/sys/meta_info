# CHANGE LOG

## 2024-10-14

### Added
- added triple tr code into satellite tbl
- added triplesat product yaml file

## 2024-10-01

### Changed
- amended the detail around the various cropmapping stages (#30)

## 2024-07-24

### Added
- added ad9, ada to sentinel-2 for Cloudbuster (#28, !20)

## 2024-07-10

### Added
- added multiple yaml files for NSW collections (#19, !17)

## 2024-06-12

### Added
- added Mixed Satellite stage code ab4: incremented age since disturbance (#14 / !13 )
## 2024-05-16

### Changed
- **filename_codes** page now up to date with last version on old wiki host (See issue #13, MR !11). 
- Prevent CI pipeline from running when creating a new MR (See MR !11).

## 2024-03-01

### Added
- Added earthi stage code config table and updated table link (see issue #3 / MR !4).


## 2024-02-22

### Added
- Added am* stage (reserved for SLATS deep learning development) to Sentinel-2 config table (see issue #5, MR !3).


## <date format: 2020-03-19>

### Added/Removed
- Description of QVF config change (see issue #999, MR !999).

### Changed/Fixed
- Some software related fix/change (see issue #999, MR !999)
- Some QVF config related fix/change (see issue #999, MR !999)
