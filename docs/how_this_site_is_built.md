## Markdown as the format of choice

To simplify editing and deployment of pages this project uses markdown as the source format. Pages are hosted on
[https://jrsrp.gitlab.io/sys/meta_info/](https://jrsrp.gitlab.io/sys/meta_info/) and the code for the information 
and documentation are on [https://gitlab.com/jrsrp/sys/meta_info](https://gitlab.com/jrsrp/sys/meta_info).

[MkDocs](https://www.mkdocs.org/) is the backend for static webpage generation.<br>
See [contributing page](https://jrsrp.gitlab.io/sys/meta_info/CONTRIBUTING/) for instructions on how you can modify content and/or add new pages to the website.

## Converting files from Dokuwiki markup to markdown

This sections illustrates some examples for migrating [old DokuWiki](https://rsc-wiki.dnr.qld.gov.au) pages into this project.

Example using Pandoc to handle the conversion from DokuWiki format to markdown_github:

* First get the target doku wiki page(s):

```shell
# from a backup archive

$ >tar -tvzf .../rsc_wiki.tar.gz | grep filename_codes.txt
./data/pages/rsc/data_prep/filename_codes.txt

cd <destination_folder>
tar -xvzf <...>/wiki_bak/rsc_wiki.tar.gz ./data/pages/rsc/data_prep/filename_codes.txt
```

```shell
# exporting directly from original webpage
# for the case of qld wiki we need to ignore SSL certificate verification.
# https://www.dokuwiki.org/export
curl -vv -k --noproxy '*' \
  "https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:filename_codes" \
  -H "X-DokuWiki-Do: export_raw" \
  -o filename_codes.txt
```
* Converting  to markdown_github

```shell
# now use docker container to handle conversion
# https://hub.docker.com/r/pandoc/minimal
$ docker run --rm \
    --volume "$(pwd):/data" \
    --user $(id -u):$(id -g) \
    pandoc/minimal -f dokuwiki -t markdown_github filename_codes.txt -o filename_codes.md
```

## Exporting old DokuWiki tables into yaml configuration files

The past wiki used DokuWiki markup tables to convey important metadata information that was used and shared amongst JRSRP members.

This project manages key metadata information using configuration files that can then be easily managed,<br>
shared, inspected and incorporated into JRSRP systems.

We chose the [YAML format](https://pyyaml.org/wiki/PyYAMLDocumentation) given its human readability advantages over JSON and XML formats.<br>
If interested, you can refer to [this nice overview of YMAL](https://realpython.com/python-yaml/#comparison-with-xml-and-json).

The conversion from DokuWiki markup to markdown step helps with getting our webpage in its initial state.

However, one then needs to:

* Do some cleanup of any leftover syntax that is not supported by the [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html).

* But most importantly, decide which parts of the page qualify/warrant the conversion as yaml configuration files,<br>
for more details see [contributing page](https://jrsrp.gitlab.io/sys/meta_info/CONTRIBUTING/).



### Web scraping script to generate yaml files

The project includes a script to extract and convert original markup tables by means of HTML scraping
the (old) published wiki page.

We'll walk through an example for scraping some of the tables on:

- https://rsc-wiki.dnr.qld.gov.au/doku.php?id=rsc:data_prep:filename_codes

We decided that the following (initial) tables available on this page warrant conversion to yaml config files:

* Satellite convention.
* Mission instrument convention.
* Instrument product convention.
* Map projection convention.
* Image formats file extension convention.
* USGS-landsat d-stream stage codes.
* ESA Sentinel-2 stage codes.

```shell
❯ ./wiki-web-info.py export-table --help
Usage: wiki-web-info.py export-table [OPTIONS] TABLE_ID...

  Given a tuple argument with:

      - html tag type name.

      - string to search for within html tag.

  export the target table to a yaml file.

  Args:

      table_id (tuple[str]): tuple, see description above.

      out_yml (str): output filename.

Options:
  --out-yml TEXT  Name of output file name. If None, it will use same prefix
                  from table id search string in HTML tag.
  -h, --help      Show this message and exit.
```

[HTML tag element reference](https://www.w3schools.com/tags/)

Example usage for converting the above tables to yaml files:

```shell
./wiki-web-info.py export-table p 'satellite (ss)'
./wiki-web-info.py export-table p 'instrument (ii)'
./wiki-web-info.py export-table p 'product (pp)'
./wiki-web-info.py export-table h2 'map projection (pp)'
./wiki-web-info.py export-table h2 'suffix'
./wiki-web-info.py export-table h3 'usgs landsat stage codes'
./wiki-web-info.py export-table h3 'sentinel-2 stages'

# creates the following yaml config files
❯ ls *_tbl.yml
instrument_ii_tbl.yml
map_projection_pp_tbl.yml
product_pp_tbl.yml
satellite_ss_tbl.yml
sentinel_2_stages_tbl.yml
suffix_tbl.yml
usgs_landsat_stage_codes_tbl.yml
```
